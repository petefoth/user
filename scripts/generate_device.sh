#!/bin/bash
DEVICE_ARCHITECTURE=${6}
DEVICE_BATTERY=${7}
DEVICE_BLUETOOTH=${8}
DEVICE_CAMERAS=${9}
DEVICE_CHANNELS=${10}
DEVICE_CODENAME=${1}
DEVICE_CPU=${11}
DEVICE_CPU_CORES=${12}
DEVICE_CPU_FREQ=${13}
DEVICE_CURRENT_BRANCH=${14}
DEVICE_DEPTH=${15}
DEVICE_DOWNLOAD_BOOT=${16}
DEVICE_GPU=${17}
DEVICE_HEIGHT=${18}
DEVICE_IMAGE=${19}
DEVICE_KERNEL=${20}
DEVICE_MAINTAINERS=${21}
DEVICE_MODELS=${22}
DEVICE_NAME=${2}
DEVICE_NETWORK=${23}
DEVICE_PERIPHERALS=${24}
DEVICE_RAM=${25}
DEVICE_RECOVERY_BOOT=${26}
DEVICE_RELEASE=${27}
DEVICE_SCREEN=${28}
DEVICE_SCREEN_PPI=${29}
DEVICE_SCREEN_RES=${30}
DEVICE_SCREEN_TECH=${31}
DEVICE_SDCARD=${32}
DEVICE_SOC=${33}
DEVICE_STORAGE=${34}
DEVICE_TREE=${35}
DEVICE_TYPE=${36}
DEVICE_VENDOR=${3}
DEVICE_VERSIONS=${4}
DEVICE_WIDTH=${37}
DEVICE_WIFI=${38}
DEVICE_STATUS=${5}

DEVICE_FOLDER="_data/devices"
INFO_FOLDER="pages/info"
INSTALL_FOLDER="pages/install"
BUILD_FOLDER="pages/build"
DEVICE_PATH=${DEVICE_FOLDER}/${DEVICE_CODENAME}.yml
INFO_PATH=${INFO_FOLDER}/${DEVICE_CODENAME}.md
INSTALL_PATH=${INSTALL_FOLDER}/${DEVICE_CODENAME}.md
BUILD_PATH=${BUILD_FOLDER}/${DEVICE_CODENAME}.md
SCRIPT_ALL_DEVICES_PATH="scripts/run_all_devices.sh"

mkdir -p ${DEVICE_FOLDER}
mkdir -p ${INFO_FOLDER}
mkdir -p ${INSTALL_FOLDER}
mkdir -p ${BUILD_FOLDER}

if [ ! -f $DEVICE_PATH ]; then
  cat scripts/templates/device.yml > $DEVICE_PATH
  sed -i "s/{architecture}/$DEVICE_ARCHITECTURE/g" $DEVICE_PATH
  sed -i "s/{battery}/$DEVICE_BATTERY/g" $DEVICE_PATH
  sed -i "s/{bluetooth}/$DEVICE_BLUETOOTH/g" $DEVICE_PATH
  sed -i "s/{cameras}/$DEVICE_CAMERAS/g" $DEVICE_PATH
  sed -i "s/{channels}/$DEVICE_CHANNELS/g" $DEVICE_PATH
  sed -i "s/{codename}/$DEVICE_CODENAME/g" $DEVICE_PATH
  sed -i "s/{cpu}/$DEVICE_CPU/g" $DEVICE_PATH
  sed -i "s/{cpu_cores}/$DEVICE_CPU_CORES/g" $DEVICE_PATH
  sed -i "s/{cpu_freq}/$DEVICE_CPU_FREQ/g" $DEVICE_PATH
  sed -i "s/{current_branch}/$DEVICE_CURRENT_BRANCH/g" $DEVICE_PATH
  sed -i "s/{depth}/$DEVICE_DEPTH/g" $DEVICE_PATH
  sed -i "s/{download_boot}/$DEVICE_DOWNLOAD_BOOT/g" $DEVICE_PATH
  sed -i "s/{gpu}/$DEVICE_GPU/g" $DEVICE_PATH
  sed -i "s/{height}/$DEVICE_HEIGHT/g" $DEVICE_PATH
  sed -i "s/{image}/$DEVICE_IMAGE/g" $DEVICE_PATH
  sed -i "s/{kernel}/$DEVICE_KERNEL/g" $DEVICE_PATH
  sed -i "s/{maintainers}/$DEVICE_MAINTAINERS/g" $DEVICE_PATH
  sed -i "s/{models}/$DEVICE_MODELS/g" $DEVICE_PATH
  sed -i "s/{name}/$DEVICE_NAME/g" $DEVICE_PATH
  sed -i "s/{network}/$DEVICE_NETWORK/g" $DEVICE_PATH
  sed -i "s/{peripherals}/$DEVICE_PERIPHERALS/g" $DEVICE_PATH
  sed -i "s/{ram}/$DEVICE_RAM/g" $DEVICE_PATH
  sed -i "s/{recovery_boot}/$DEVICE_RECOVERY_BOOT/g" $DEVICE_PATH
  sed -i "s/{release}/$DEVICE_RELEASE/g" $DEVICE_PATH
  sed -i "s/{screen}/$DEVICE_SCREEN/g" $DEVICE_PATH
  sed -i "s/{screen_ppi}/$DEVICE_SCREEN_PPI/g" $DEVICE_PATH
  sed -i "s/{screen_res}/$DEVICE_SCREEN_RES/g" $DEVICE_PATH
  sed -i "s/{screen_tech}/$DEVICE_SCREEN_TECH/g" $DEVICE_PATH
  sed -i "s/{sdcard}/$DEVICE_SDCARD/g" $DEVICE_PATH
  sed -i "s/{soc}/$DEVICE_SOC/g" $DEVICE_PATH
  sed -i "s/{storage}/$DEVICE_STORAGE/g" $DEVICE_PATH
  sed -i "s/{tree}/$DEVICE_TREE/g" $DEVICE_PATH
  sed -i "s/{type}/$DEVICE_TYPE/g" $DEVICE_PATH
  sed -i "s/{vendor}/$DEVICE_VENDOR/g" $DEVICE_PATH
  sed -i "s/{versions}/$DEVICE_VERSIONS/g" $DEVICE_PATH
  sed -i "s/{width}/$DEVICE_WIDTH/g" $DEVICE_PATH
  sed -i "s/{wifi}/$DEVICE_WIFI/g" $DEVICE_PATH
  sed -i "s/{status}/$DEVICE_STATUS/g" $DEVICE_PATH
fi

if [ ! -f $INFO_PATH ]; then
  cat scripts/templates/info.md > $INFO_PATH
  sed -i "s/{architecture}/$DEVICE_ARCHITECTURE/g" $INFO_PATH
  sed -i "s/{battery}/$DEVICE_BATTERY/g" $INFO_PATH
  sed -i "s/{bluetooth}/$DEVICE_BLUETOOTH/g" $INFO_PATH
  sed -i "s/{cameras}/$DEVICE_CAMERAS/g" $INFO_PATH
  sed -i "s/{channels}/$DEVICE_CHANNELS/g" $INFO_PATH
  sed -i "s/{codename}/$DEVICE_CODENAME/g" $INFO_PATH
  sed -i "s/{cpu}/$DEVICE_CPU/g" $INFO_PATH
  sed -i "s/{cpu_cores}/$DEVICE_CPU_CORES/g" $INFO_PATH
  sed -i "s/{cpu_freq}/$DEVICE_CPU_FREQ/g" $INFO_PATH
  sed -i "s/{current_branch}/$DEVICE_CURRENT_BRANCH/g" $INFO_PATH
  sed -i "s/{depth}/$DEVICE_DEPTH/g" $INFO_PATH
  sed -i "s/{download_boot}/$DEVICE_DOWNLOAD_BOOT/g" $INFO_PATH
  sed -i "s/{gpu}/$DEVICE_GPU/g" $INFO_PATH
  sed -i "s/{height}/$DEVICE_HEIGHT/g" $INFO_PATH
  sed -i "s/{image}/$DEVICE_IMAGE/g" $INFO_PATH
  sed -i "s/{kernel}/$DEVICE_KERNEL/g" $INFO_PATH
  sed -i "s/{maintainers}/$DEVICE_MAINTAINERS/g" $INFO_PATH
  sed -i "s/{models}/$DEVICE_MODELS/g" $INFO_PATH
  sed -i "s/{name}/$DEVICE_NAME/g" $INFO_PATH
  sed -i "s/{network}/$DEVICE_NETWORK/g" $INFO_PATH
  sed -i "s/{peripherals}/$DEVICE_PERIPHERALS/g" $INFO_PATH
  sed -i "s/{ram}/$DEVICE_RAM/g" $INFO_PATH
  sed -i "s/{recovery_boot}/$DEVICE_RECOVERY_BOOT/g" $INFO_PATH
  sed -i "s/{release}/$DEVICE_RELEASE/g" $INFO_PATH
  sed -i "s/{screen}/$DEVICE_SCREEN/g" $INFO_PATH
  sed -i "s/{screen_ppi}/$DEVICE_SCREEN_PPI/g" $INFO_PATH
  sed -i "s/{screen_res}/$DEVICE_SCREEN_RES/g" $INFO_PATH
  sed -i "s/{screen_tech}/$DEVICE_SCREEN_TECH/g" $INFO_PATH
  sed -i "s/{sdcard}/$DEVICE_SDCARD/g" $INFO_PATH
  sed -i "s/{soc}/$DEVICE_SOC/g" $INFO_PATH
  sed -i "s/{storage}/$DEVICE_STORAGE/g" $INFO_PATH
  sed -i "s/{tree}/$DEVICE_TREE/g" $INFO_PATH
  sed -i "s/{type}/$DEVICE_TYPE/g" $INFO_PATH
  sed -i "s/{vendor}/$DEVICE_VENDOR/g" $INFO_PATH
  sed -i "s/{versions}/$DEVICE_VERSIONS/g" $INFO_PATH
  sed -i "s/{width}/$DEVICE_WIDTH/g" $INFO_PATH
  sed -i "s/{wifi}/$DEVICE_WIFI/g" $INFO_PATH
  sed -i "s/{status}/$DEVICE_STATUS/g" $INFO_PATH
fi

if [ ! -f $INSTALL_PATH ]; then
  cat scripts/templates/install.md > $INSTALL_PATH
  sed -i "s/{architecture}/$DEVICE_ARCHITECTURE/g" $INSTALL_PATH
  sed -i "s/{battery}/$DEVICE_BATTERY/g" $INSTALL_PATH
  sed -i "s/{bluetooth}/$DEVICE_BLUETOOTH/g" $INSTALL_PATH
  sed -i "s/{cameras}/$DEVICE_CAMERAS/g" $INSTALL_PATH
  sed -i "s/{channels}/$DEVICE_CHANNELS/g" $INSTALL_PATH
  sed -i "s/{codename}/$DEVICE_CODENAME/g" $INSTALL_PATH
  sed -i "s/{cpu}/$DEVICE_CPU/g" $INSTALL_PATH
  sed -i "s/{cpu_cores}/$DEVICE_CPU_CORES/g" $INSTALL_PATH
  sed -i "s/{cpu_freq}/$DEVICE_CPU_FREQ/g" $INSTALL_PATH
  sed -i "s/{current_branch}/$DEVICE_CURRENT_BRANCH/g" $INSTALL_PATH
  sed -i "s/{depth}/$DEVICE_DEPTH/g" $INSTALL_PATH
  sed -i "s/{download_boot}/$DEVICE_DOWNLOAD_BOOT/g" $INSTALL_PATH
  sed -i "s/{gpu}/$DEVICE_GPU/g" $INSTALL_PATH
  sed -i "s/{height}/$DEVICE_HEIGHT/g" $INSTALL_PATH
  sed -i "s/{image}/$DEVICE_IMAGE/g" $INSTALL_PATH
  sed -i "s/{kernel}/$DEVICE_KERNEL/g" $INSTALL_PATH
  sed -i "s/{maintainers}/$DEVICE_MAINTAINERS/g" $INSTALL_PATH
  sed -i "s/{models}/$DEVICE_MODELS/g" $INSTALL_PATH
  sed -i "s/{name}/$DEVICE_NAME/g" $INSTALL_PATH
  sed -i "s/{network}/$DEVICE_NETWORK/g" $INSTALL_PATH
  sed -i "s/{peripherals}/$DEVICE_PERIPHERALS/g" $INSTALL_PATH
  sed -i "s/{ram}/$DEVICE_RAM/g" $INSTALL_PATH
  sed -i "s/{recovery_boot}/$DEVICE_RECOVERY_BOOT/g" $INSTALL_PATH
  sed -i "s/{release}/$DEVICE_RELEASE/g" $INSTALL_PATH
  sed -i "s/{screen}/$DEVICE_SCREEN/g" $INSTALL_PATH
  sed -i "s/{screen_ppi}/$DEVICE_SCREEN_PPI/g" $INSTALL_PATH
  sed -i "s/{screen_res}/$DEVICE_SCREEN_RES/g" $INSTALL_PATH
  sed -i "s/{screen_tech}/$DEVICE_SCREEN_TECH/g" $INSTALL_PATH
  sed -i "s/{sdcard}/$DEVICE_SDCARD/g" $INSTALL_PATH
  sed -i "s/{soc}/$DEVICE_SOC/g" $INSTALL_PATH
  sed -i "s/{storage}/$DEVICE_STORAGE/g" $INSTALL_PATH
  sed -i "s/{tree}/$DEVICE_TREE/g" $INSTALL_PATH
  sed -i "s/{type}/$DEVICE_TYPE/g" $INSTALL_PATH
  sed -i "s/{vendor}/$DEVICE_VENDOR/g" $INSTALL_PATH
  sed -i "s/{versions}/$DEVICE_VERSIONS/g" $INSTALL_PATH
  sed -i "s/{width}/$DEVICE_WIDTH/g" $INSTALL_PATH
  sed -i "s/{wifi}/$DEVICE_WIFI/g" $INSTALL_PATH
  sed -i "s/{status}/$DEVICE_STATUS/g" $INSTALL_PATH
fi

if [ ! -f $BUILD_PATH ]; then
  cat scripts/templates/build.md > $BUILD_PATH
  sed -i "s/{architecture}/$DEVICE_ARCHITECTURE/g" $BUILD_PATH
  sed -i "s/{battery}/$DEVICE_BATTERY/g" $BUILD_PATH
  sed -i "s/{bluetooth}/$DEVICE_BLUETOOTH/g" $BUILD_PATH
  sed -i "s/{cameras}/$DEVICE_CAMERAS/g" $BUILD_PATH
  sed -i "s/{channels}/$DEVICE_CHANNELS/g" $BUILD_PATH
  sed -i "s/{codename}/$DEVICE_CODENAME/g" $BUILD_PATH
  sed -i "s/{cpu}/$DEVICE_CPU/g" $BUILD_PATH
  sed -i "s/{cpu_cores}/$DEVICE_CPU_CORES/g" $BUILD_PATH
  sed -i "s/{cpu_freq}/$DEVICE_CPU_FREQ/g" $BUILD_PATH
  sed -i "s/{current_branch}/$DEVICE_CURRENT_BRANCH/g" $BUILD_PATH
  sed -i "s/{depth}/$DEVICE_DEPTH/g" $BUILD_PATH
  sed -i "s/{download_boot}/$DEVICE_DOWNLOAD_BOOT/g" $BUILD_PATH
  sed -i "s/{gpu}/$DEVICE_GPU/g" $BUILD_PATH
  sed -i "s/{height}/$DEVICE_HEIGHT/g" $BUILD_PATH
  sed -i "s/{image}/$DEVICE_IMAGE/g" $BUILD_PATH
  sed -i "s/{kernel}/$DEVICE_KERNEL/g" $BUILD_PATH
  sed -i "s/{maintainers}/$DEVICE_MAINTAINERS/g" $BUILD_PATH
  sed -i "s/{models}/$DEVICE_MODELS/g" $BUILD_PATH
  sed -i "s/{name}/$DEVICE_NAME/g" $BUILD_PATH
  sed -i "s/{network}/$DEVICE_NETWORK/g" $BUILD_PATH
  sed -i "s/{peripherals}/$DEVICE_PERIPHERALS/g" $BUILD_PATH
  sed -i "s/{ram}/$DEVICE_RAM/g" $BUILD_PATH
  sed -i "s/{recovery_boot}/$DEVICE_RECOVERY_BOOT/g" $BUILD_PATH
  sed -i "s/{release}/$DEVICE_RELEASE/g" $BUILD_PATH
  sed -i "s/{screen}/$DEVICE_SCREEN/g" $BUILD_PATH
  sed -i "s/{screen_ppi}/$DEVICE_SCREEN_PPI/g" $BUILD_PATH
  sed -i "s/{screen_res}/$DEVICE_SCREEN_RES/g" $BUILD_PATH
  sed -i "s/{screen_tech}/$DEVICE_SCREEN_TECH/g" $BUILD_PATH
  sed -i "s/{sdcard}/$DEVICE_SDCARD/g" $BUILD_PATH
  sed -i "s/{soc}/$DEVICE_SOC/g" $BUILD_PATH
  sed -i "s/{storage}/$DEVICE_STORAGE/g" $BUILD_PATH
  sed -i "s/{tree}/$DEVICE_TREE/g" $BUILD_PATH
  sed -i "s/{type}/$DEVICE_TYPE/g" $BUILD_PATH
  sed -i "s/{vendor}/$DEVICE_VENDOR/g" $BUILD_PATH
  sed -i "s/{versions}/$DEVICE_VERSIONS/g" $BUILD_PATH
  sed -i "s/{width}/$DEVICE_WIDTH/g" $BUILD_PATH
  sed -i "s/{wifi}/$DEVICE_WIFI/g" $BUILD_PATH
  sed -i "s/{status}/$DEVICE_STATUS/g" $BUILD_PATH
fi

if [ ! -f $SCRIPT_ALL_DEVICES_PATH ]; then
  echo "#/bin/bash" > $SCRIPT_ALL_DEVICES_PATH
  chmod u+x $SCRIPT_ALL_DEVICES_PATH
fi

cat $SCRIPT_ALL_DEVICES_PATH | grep $DEVICE_CODENAME
if [ $? != 0 ]; then
  echo "./scripts/generate_device.sh \"${DEVICE_ARCHITECTURE}\" \"${DEVICE_BATTERY}\" \"${DEVICE_BLUETOOTH}\" \"${DEVICE_CAMERAS}\" \"${DEVICE_CHANNELS}\" \"${DEVICE_CODENAME}\" \"${DEVICE_CPU}\" \"${DEVICE_CPU_CORES}\" \"${DEVICE_CPU_FREQ}\" \"${DEVICE_CURRENT_BRANCH}\" \"${DEVICE_DEPTH}\" \"${DEVICE_DOWNLOAD_BOOT}\" \"${DEVICE_GPU}\" \"${DEVICE_HEIGHT}\" \"${DEVICE_IMAGE}\" \"${DEVICE_KERNEL}\" \"${DEVICE_MAINTAINERS}\" \"${DEVICE_MODELS}\"\"${DEVICE_NAME}\" \"${DEVICE_NETWORK}\" \"${DEVICE_PERIPHERALS}\" \"${DEVICE_RAM}\" \"${DEVICE_RECOVERY_BOOT}\" \"${DEVICE_RELEASE}\" \"${DEVICE_SCREEN}\" \"${DEVICE_SCREEN_PPI}\" \"${DEVICE_SCREEN_RES}\" \"${DEVICE_SCREEN_TECH}\" \"${DEVICE_SDCARD}\" \"${DEVICE_SOC}\" \"${DEVICE_STORAGE}\" \"${DEVICE_TREE}\" \"${DEVICE_TYPE}\" \"${DEVICE_VENDOR}\"\"${DEVICE_VERSIONS}\" \"${DEVICE_WIDTH}\" \"${DEVICE_WIFI}\" \"${DEVICE_STATUS}\"" >> scripts/run_all_devices.sh
fi
