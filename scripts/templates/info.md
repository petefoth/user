---
sidebar: home_sidebar
title: Info about {vendor} {name} - {codename}
folder: info
layout: default
permalink: /devices/{codename}
device: {codename}
---
{% include templates/device_info.md %}
