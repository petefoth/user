Flash your device only if you **know** what you are doing and are OK taking the **associated risk**.
The /e/ project and its project members deny any and all responsibility about the consequences of using /e/ software and or /e/ services.
