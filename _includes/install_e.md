## Installing /e/ from recovery

1.&nbsp;Before proceeding ensure you have downloaded the /e/OS for your device, from the link given in the **Downloads** section above

2.&nbsp;If you aren’t already in recovery mode, reboot into recovery mode by typing the below command in the console of an adb enabled PC

```shell
adb reboot recovery
```
{% include alerts/tip.html content="You can also do this manually using the below key combination" %}
- {{ device.recovery_boot }}

3.&nbsp;In TWRP return to main menu, then tap `Wipe`

4.&nbsp;Now tap `Format Data` and continue with the formatting process. 

Here a new screen may show where you will have to type `Yes' and confirm the action by clicking a button.

{% include alerts/danger.html content="Format Data will remove encryption as well as **delete all files** stored on the internal storage. Remember to take a backup first. Check **Requirements** section above."%}

5.&nbsp;Return to the previous menu and tap `Advanced Wipe`.

6.&nbsp;Select the `Cache` and `System` partitions to be wiped and then `Swipe to Wipe`

{% if device.install_without_sideload %}
7.&nbsp;Push and install the /e/ .zip package:
```shell
adb push </e/ zip package> /sdcard/
adb shell twrp install /sdcard/</e/ zip package>
adb shell rm /sdcard/</e/ zip package>
```
{% else %}
7.&nbsp;Sideload the /e/ .zip package.
{% include sideload.md %}
{% endif %}
8.&nbsp;Once installation has finished, return to the main menu, tap Reboot, and then System

{% include alerts/warning.html content="Avoid installing any additional apps or services if suggested by the recovery. They may cause your device to bootloop, as well as attempt to access and or corrupt your data." %}
