### Getting a `fastboot` error on the FP3 ?

-  On some versions of `fastboot` you may get an error while running the `Fastboot flash boot boot.img` command

- The below error may show up on the console

`Fastboot error: couldn’t parse partition size ‘0x’`

- A workaround for this error is as under. Run this command in the console:
```
Fastboot getvar current-slot
```
- This would show the current active slot. For e.g. if it shows `a` run the below command
```
Fastboot --set-active=b
Fastboot flash boot_b boot.img
```