> **Warning** : Downgrading Smartphones already on Android Q or LineageOS 17.x to Pie or from Pie OS to Oreo or Nougat can cause instability or at worst brick some devices.
