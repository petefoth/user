{%- assign device = site.data.devices[page.device] -%}
{% if device.custom_recovery_codename %}
{% assign custom_recovery_codename = device.custom_recovery_codename %}
{% else %}
{% assign custom_recovery_codename = device.codename %}
{% endif %}

## Temporarily Booting a custom recovery using `fastboot`

1.&nbsp;Connect your device to your PC via USB.

2.&nbsp;On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
```  
adb reboot bootloader
```  

3.&nbsp;Once the device is in fastboot mode, verify your PC finds it by typing:
```
fastboot devices
```   
{% if device.fastboot_boot %}
4.&nbsp;Temporarily boot a recovery on your device by typing
```   
fastboot boot twrp-x.x.x-x-{{ device.codename}}.img
```    
{% else %}
4.&nbsp;Temporarily flash a recovery on your device by typing:
```    
fastboot flash recovery <recovery_filename>.img
``` 
on some devices the below command may be required
```
fastboot boot <recovery_filename>.img
``` 
or
```  
fastboot flash boot <recovery_filename>.img
```    
5.&nbsp;Manually reboot into recovery mode


{% endif %}
