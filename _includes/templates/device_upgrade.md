<div class="wrapper" markdown="1">
{% assign device = site.data.devices[page.device] %}

## Manually upgrade {{ device.codename }} to {{ device.upgrade_available }}


{% if device.type == "phone" or device.type == "Phone" %}
  {% include alerts/warning_prevent_os_downgrade.md %}
{% endif%}

{% include alerts/suggestions.html %}


### Basic Requirements

* adb and fastbooot should be setup and working on your PC
  > To set up adb follow details provided [here](../../pages/install-adb)

* adb and fastboot need root rights in Linux and administrator rights in Windows.
  > In Linux prepend sudo to any adb or fastboot command For e.g. “sudo fastboot reboot” and
    in Windows start the command window as administrator.
{% if device.vendor == "Samsung" or  device.vendor == "Xiaomi" %}
{% include alerts/upgrade_tostock.html %}
{% endif %}

{% if device.is_beta %}
### Warning:

  > - Please note this upgrade build for the {{ device.codename }} could not be tested as there were no device specific testers available.
  > - Some devices may need a revert to stock ROM prior to upgrade.
  > - To flash the stock ROM you would need to use a tool provided by your vendor
  > - You may also need to download TWRP built specifically for your device.


{% endif %}

### Download

* Download Upgrade Build for the {{ device.codename }} here
  {%- for status in device.status %}
  [{{status}}](https://images.ecloud.global/{{status}}/{{device.codename}}/) ({{device.upgrade_available}})
  {%- endfor %}

* Download [TWRP](https://dl.twrp.me/{{device.codename}})
  {% if device.twrp_version %}

> The TWRP version we tested was {{ device.twrp_version }}

  {% endif %}

> Download of TWRP is not required if you already have it running on your {{ device.codename }}

{% if device.stockupgrade %}
* Download the latest stock ROM build for the {{ device.codename }}
  > stock ROM tested {{ device.stockupgrade }}

{% if device.stockupgrade_tool %}

* Download the {{ device.stockupgrade_tool }}
   > tool version tested {{ device.stockupgrade_toolver }}

{% endif %}
- Follow the steps to install the stock ROM as given on the vendor site

- Once the Stock ROM is flashed check if it is working for e.g receiving and making calls, audio, FMRadio ..

- Next enable developer options on the phone 

- Ensure the device bootloader is unlocked. Flashing stock ROM can lock the bootloader on some devices.
  > Follow the device unlock guidelines as prescibed by your vendor. For e.g. on Xiaomi devices it would require running the Mi Unlock tool. Since the device was already unlocked the process should take a couple of minutes only.


{% endif %}

### Steps to Flash the Upgrade /e/OS ROM


1. Open a command window and change into the directory where you downloaded the files

1. Take a complete backup of your phone data
1. Ensure your {{ device.codename }} smartphone is fully charged
1. Remove any pattern or PIN locks you may have set on your phone
  > you can set these up after the OS has been successfully installed on your phone

1. If you already have TWRP recovery installed on your device
    - skip steps 6 and 7
    - Go directly into TWRP recovery (step 9)
    - To do that reboot your device and during startup follow the steps as given here
    - {{ device.recovery_boot }}

1. To flash TWRP recovery
   - Reboot your device and press volume-down during startup.
   - This will bring the device into fastboot mode

1. Flash the TWRP image in the fastboot mode by running below command in the PC
    ```
    fastboot flash recovery twrpimagename.img
    ```
    on some devices the below command may be required
    ```
    fastboot boot twrpimagename.img
    ```
   > replace the twrpimagename.img with the name of the twrp image file name you downloaded previously
   > the boot command will flash a temporary TWRP image on your smartphone

    {% if device.twrp_version %}
    > The TWRP version we tested was {{ device.twrp_version }}

    {% endif %}

1. Reboot the device via the command
   ```
   fastboot reboot
   ```

   and press volume-up while booting to go into TWRP recovery

1. If your device is un-encrypted
   - Continue with installing the new build as given in step 13


1. If your device is encrypted
   - Ensure you have taken a backup as proposed in step 2
   - Continue to step 11

1. In TWRP wipe screen on your device click the format data button on the lower side to remove encryption

1. Select the Wipe button and in the wipe screen select system + data + cache and swipe to wipe

1. Sideload the /e/ .zip package

    - On the device, In TWRP select

     Advanced >> ADB Sideload >> then swipe to begin sideload

    - On the host machine, sideload the package by running the below command
```
	adb sideload /e/ OS image name.zip
```
     >replace the /e/ OS image name.zip with the name of the /e/ OS zip file name,
     e.g. adb sideload e-0.7-p-2020011437643-test-FP2.zip

1. Once the installation is successfully completed reboot system from the TWRP screen.

1. Your {{ device.codename}} should now be running the  {{ device.upgrade_available}} version of /e/..enjoy !!!


### Answers to some questions you may have on the process
#### Why do I need to manually upgrade the OS
> {% include alerts/warning_why_manually_upgrade.md %}
{%  if device.stockupgrade %}
#### Why do I need to flash Stock ROM before flashing the /e/ROM to upgrade?
  > {% include alerts/warning_why_stock_upgrade.md %}
{% endif %}
