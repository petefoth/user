{% assign device = site.data.devices[page.device] %}
<div  class="container">
  <div class="row">
    <div class="offset-md-2 col-md-4 col-sm-2">
      {% include device/device_left.html %}
    </div>
    <div class="col-md-6 col-sm-2">
     {% include device/device_right.html %}
    </div>
  </div>
  <div class="row">
    <div class="col">
    {% include device/device_info_disclaimer.html %}
    </div>
  </div>
</div>