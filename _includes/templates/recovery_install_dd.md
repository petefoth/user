{%- assign device = site.data.devices[page.device] -%}
{% if device.custom_recovery_codename %}
{% assign custom_recovery_codename = device.custom_recovery_codename %}
{% else %}
{% assign custom_recovery_codename = device.codename %}
{% endif %}

## Rooting your device

> Important: The device must be rooted before proceeding any further.

{% case device.root_method[0] %}
{% when 'custom' %}
1. Root your device by following [this]({{ device.root_method[1] }}) guide.
{% when 'kingroot' %}
1. Download KingRoot from [here](https://kingroot.net/).
   1. Install and run the apk to achieve root. Ensure you have a working Internet connection.
{% when 'towelroot' %}
1. Download TowelRoot from [here](https://towelroot.com/).
   1. Click the large lambda symbol to download the apk.
   1. Install and run the apk to achieve root.
{% endcase %}

## Installing a custom recovery using `dd`

{% include alerts/warning_install_custom_recovery.md %}

1. Place the recovery image file on the root of /sdcard (USB debugging have to be enabled in developer options) :

    ```shell
    adb push twrp-x.x.x-x-{{ device.codename }}.img /sdcard/twrp-{{ device.codename }}.img
    ```
1. Now, open an `adb shell` from a command prompt (on Windows) or terminal (on Linux or macOS) window. In that shell, type the following commands:

    ```shell
    su
    dd if=/sdcard/twrp-{{ device.codename }}.img of=/dev/block/platform/msm_sdcc.1/by-name/recovery
    ```
1. Manually reboot into recovery
   With the device powered off, hold `Volume Down` + `Power`. Release all buttons and press `Power` again as soon as the LG logo appears.

{% if device.vendor == "LG" %}
1. Accept the factory reset prompt using the hardware buttons. If you have done everything correctly, this will not actually reset your device but instead will install the custom recovery.
{% endif %}
