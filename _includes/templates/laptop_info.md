{% assign laptop = site.data.laptops[page.laptop] %}
<div  class="container">
  <div class="row">
    <div class="offset-md-2 col-md-4 col-sm-2">
        {% include device/laptop_left.html %}
    </div>
    <div class="col-md-6 col-sm-2" >
          {% include device/laptop_right.html %}
    </div>
  </div>
  <div class="row">
    <div class="col">
    {% include device/device_info_disclaimer.html %}
    </div>
  </div>
</div> 
 

