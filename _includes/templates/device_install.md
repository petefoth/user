<div class="wrapper" markdown="1">
{% assign device = site.data.devices[page.device] %}

# Install {%- if device.minimal_os %} /e/ minimal {%- else %} /e/ {%- endif %}  on a {{ device.vendor }} {{ device.name }} - "{{ device.codename }}" {%- if device.is_beta %} (beta) {%- endif %}


{% include alerts/combined_warnings.html %}

{% include alerts/basic_requirements.html %}

{% if device.upgrade_available %}
{% if device.vendor == "Samsung" or  device.vendor == "Xiaomi" %}
{% include alerts/upgrade_tostock.html %}
{% endif %}
{% endif %}

{% include templates/device_downloads.md %}

{% if device.before_install %}
{% capture before_install %}templates/{{ device.before_install }}.md{% endcapture %}
{% include {{ before_install }} %}
{% endif %}


{% if device.install_method and device.codename != 'FP3' %}
{% capture recovery_install_method %}templates/recovery_install_{{ device.install_method }}.md{% endcapture %}
{% include {{ recovery_install_method }} %}
{% endif %}


{% if device.recovery_boot and device.uses_twrp %}
{% include alerts/tip.html content="On some devices if the device does not boot into TWRP you could try it manually by following these steps " %}
- {{ device.recovery_boot }}
{% endif %}

{%- if device.before_e_install %}
{% capture path %}templates/{{ device.before_e_install }}.md{% endcapture %}
{% include {{ path }} %}
{%- endif %}

{% if device.install_e %}
{% capture before_install %}templates/{{ device.install_e }}.md{% endcapture %}
{% include {{ before_install }} %}
{% else %}
{% include install_e.md %}
{% endif %}

{% include alerts/success.html content= "Congratulations !! Your phone should now be booting into /e/OS !! "%}

<details> 
<summary>To find some troubleshooting tips… click here</summary>
{% for cat in site.data.troubleshooting.tips %}
<h3>{{ cat.category }}</h3>
    <ul>
    {% for entry in  cat.subitems  %}
      <li>{{ entry.error }} - {{ entry.suggestion }} </li>
    {% endfor %}
    </ul>
{% endfor %}
</details>
<br/><br/>


{% if device.additional_install_guides %}

## Still need help to complete the installation ?

Check out this document: An [install Guide]({{ device.additional_install_guides }}) for the  {{ device.codename}} 

{% endif %}

{% include alerts/suggestions.html %}

{% if device.is_lineageos_official %}
{% include licence_device.md %}
{% endif %}
