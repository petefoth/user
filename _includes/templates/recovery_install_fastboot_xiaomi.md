## Unlocking the bootloader

{% include alerts/warning_unlocking_bootloader.md %}

1. Visit [Xiaomi’s official](http://en.miui.com/unlock/) unlocking website and apply for unlocking permissions.
1. Wait until you get the approval, which may take some days.
1. At the same time, link the device to your Mi account in `Settings` > `Additional settings` > `Developer options` > `Mi Unlock status`. This step is mandatory and will trigger a countdown of 360 hours, the waiting period before a device can be unlocked.
1. Once you get the approval, visit the website again and you will be redirected to the Mi Unlock app download page.
1. Download the Mi Unlock app (Windows is required to run the app).
1. Once the 360 hours waiting period is over, run the Mi Unlock app and follow the instructions provided by the app.
1. After device and Mi account are successfully verified, the bootloader should be unlocked.
1. Since the device resets completely, you will need to re-enable USB debugging to continue.

> A Mi account is required to apply for permissions. You don’t need to re-apply for permissions to unlock new devices, but beware that one account is only allowed to unlock one unique device every 30 days.


> It is highly recommended to have the latest official MIUI dev package installed on the device, before proceeding with unlock.

{% if device.is_ab_device %}
{% include templates/recovery_install_fastboot_ab.md %}
{% else %}
{% include templates/recovery_install_fastboot_generic.md %}
{% endif %}
