## Preparing for installation using Heimdall

Samsung devices come with a unique boot mode called “Download mode”, which is very similar to “Fastboot mode” on some devices with unlocked bootloaders. Heimdall is a cross-platform, open-source tool for interfacing with Download mode on Samsung devices. The preferred method of installing a custom recovery is through this boot mode – rooting the stock firmware is neither necessary nor required.

Please note: On windows PC's to run the installation using ODIN check this [HOWTO](https://community.e.foundation/t/howto-install-e-on-a-samsung-smartphone-with-windows-easily/3186). The steps given below explain how to install Heimdall and flash /e/OS

1. Enable OEM unlock in the Developer options under device Settings.
> It appears sometime that the OEM unlock option is missing from Development options, especially on new devices. Please connect your device to the wifi and check of system updates. After a reboot, the option should appear.

1. [Install Heimdall](../../how-tos/install-heimdall)  or if you prefer you can [Build latest Heimdall suite](../../how-tos/build-heimdall)
1. Power off the device and connect the USB adapter to the computer (but not to the device, yet).
1. Boot into download mode:
    * {{ device.download_boot }}

    Accept the disclaimer, then insert the USB cable into the device.
1. Windows only: install the drivers. A more complete set of instructions can be found in the ZAdiag user guide.
   1. Run `zadiag.exe` from the Drivers folder of the Heimdall suite.
   1. Choose Options » List all devices from the menu.
   1. Select Samsung USB Composite Device or MSM8x60 or Gadget Serial or Device Name from the drop down menu. (If nothing relevant appears, try uninstalling any Samsung related Windows software, like Samsung Windows drivers and/or Kies).
   1. Click Replace Driver (having to select Install Driver from the drop down list built into the
   1. If you are prompted with a warning that the installer is unable to verify the publisher of the driver, select Install this driver anyway. You may receive two more prompts about security. Select the options that allow you to carry on.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    ```shell
    heimdall print-pit
    ```
1. If the device reboots, Heimdall is installed and working properly.



{% if device.recovery_install_need_patch %}
## Patch the device

1. Download the patch from [here](https://androidfilehost.com/?fid=962187416754474353)
1. Install the patch
1. On the device, go into `Advanced` > `ADB Sideload`, then swipe to begin sideload
1. From the computer, please run `adb sideload <patch file>`
{% endif %}

## Installing a custom recovery

1. Download a custom recovery - you can download [TWRP](https://dl.twrp.me/{{ device.codename }}).
    {% if device.alternate_twrp %}
    > An [alternate location]({{ device.alternate_twrp }}) to download TWRP

    {% endif %}
    {% if device.custom_recovery_link %}
    > An alternate location to download TWRP is [this link]({{ device.custom_recovery_link }})
    {% endif %}
    {% if device.twrp_version %}
        > The version of TWRP we used to test the ROM was {{ device.twrp_version }}

    {% endif %}
1. Power off the device and connect the USB adapter to the computer (but not to the device, yet).
1. Boot into download mode:
* {{ device.download_boot }}

   Accept the disclaimer, then insert the USB cable into the device.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window in the directory the recovery image is located, and type:
```shell
heimdall flash --RECOVERY twrp-x.x.x-x-{{ device.codename }}.img --no-reboot
```
1. A blue transfer bar will appear on the device showing the recovery being transferred.
1. Unplug the USB cable from your device.
1. Manually reboot into recovery:
* {{ device.recovery_boot }}

    {% if device.update_vendors %}
    > On some devices, installation can fail if vendors are not up-to-date. In this case, please:
    > * Download [those vendors](https://androidfilehost.com/?fid=11410932744536982158)
    > * Install them
    >    * On the device, go into `Advanced` > `ADB Sideload`, then swipe to begin sideload
    >    * From the computer, please run `adb sideload <vendors file>`

    {% endif %}

    > Note: Be sure to reboot into recovery immediately after having installed the custom recovery. Otherwise the custom recovery will be overwritten and the device will reboot (appearing as though your custom recovery failed to install).
