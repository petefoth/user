<div class="wrapper" markdown="1">

On a Linux computer

    Extract the .gz archive
    Insert a MicroSD card with minimum 8 GB storage

In the konsole run the code

     sudo fdisk -l

this will display your SD card on the Konsole .

It would show up like

    /dev/sdX

{% include alerts/tip.html content="Please ensure you have selected the correct SD Card and not your Hard Disk as the next step will delete all data from it." %}

{% include alerts/warning.html content="This search can also show your main hard disk so be very careful at this point to identify the SD card." %}

Now in the console type

    sudo dd if=extacted-file-path of=/dev/sdX


where X will be the character that identifies your SD card and you got in the previous step

Open the SD card in a FileManager

There would be multiple partitions

Go to the BOOT partition

    modify the uEnv.txt file

Change the variable "pinebook_lcd_mode" to match your device

    could be batch1, batch2 or 1080p

Depending on your LCD settings.

{% include alerts/tip.html content="In case the laptop does not boot then try a different setting" %}


{% include alerts/success.html content="Your /e/OS should now boot up on your laptop...enjoy !!!" %}
