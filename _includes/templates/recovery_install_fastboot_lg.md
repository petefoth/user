## Unlocking the bootloader

{% include alerts/warning_unlocking_bootloader.md %}

1. Visit [LG’s official unlocking website](http://developer.lge.com/resource/mobile/RetrieveBootloader.dev), where you’ll be asked to login.
1. Follow the instructions and get your unlock file.
1. Connect your device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    ```shell
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing:

    ```shell
    fastboot devices
    ```
1. Now type the following command to unlock the bootloader:

    ```shell
    fastboot flash unlock unlock.bin
    ```
    Where unlock.bin is the bootloader unlock file you received in the email.
1. Wait for the bootloader unlocking process to complete. Once finished, you can check if bootloader is successfully unlocked by typing:

    ```shell
    fastboot getvar unlocked
    ```
    Verify that the response is unlocked: yes. In that case, you can now install third-party firmware.
1. Since the device resets completely, you will need to re-enable USB debugging to continue.

{% if device.is_ab_device %}
{% include templates/recovery_install_fastboot_ab.md %}
{% else %}
{% include templates/recovery_install_fastboot_generic.md %}
{% endif %}
