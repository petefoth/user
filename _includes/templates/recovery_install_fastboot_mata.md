{% assign device = site.data.devices[page.device] %}

## Unlocking the bootloader

{% include alerts/warning_unlocking_bootloader.md %}

1. Connect your device to your PC via USB.


1. Enable OEM unlock in the Developer options under device Settings, if present.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
    ```
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing:
    ```
    fastboot devices
    ```

1. Unlock bootloader

    ```shell
    fastboot flashing unlock
    ```

1. Unlock critical partition

    ```shell
    fastboot flashing unlock_critical
    ```

{% include templates/recovery_install_fastboot_ab.md %}
