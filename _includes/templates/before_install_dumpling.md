## Updating firmware
Ensure you have downloaded the latest {{ device.versions }} firmware for the {{ device.codename }} 

Now we will sideload the downloaded firmware file
{% include sideload.md %}