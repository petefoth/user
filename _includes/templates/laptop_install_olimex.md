<div class="wrapper" markdown="1">

On a Linux computer

    Extract the .gz archive
    Insert a MicroSD card with minimum 8 GB storage

In the konsole run the code

     sudo fdisk -l

this will display your SD card on the Konsole .

It would show up like

    /dev/sdX

{% include alerts/tip.html content="Please ensure you have selected the correct SD Card and not your Hard Disk as the next step will delete all data from it." %}

{% include alerts/warning.html content="This search can also show your main hard disk so be very careful at this point to identify the SD card." %}


Now in the console type

    sudo dd if=extacted-file-path of=/dev/sdX


where X will be the character that identifies your SD card and you got in the previous step

Insert the SDCard on your {{ device.codename }} laptop

Set the laptop to boot of the SDCard and reboot

{% include alerts/success.html content="Your /e/OS should now boot up on your laptop...enjoy !!!" %}
