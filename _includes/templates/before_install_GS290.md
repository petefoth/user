## If installing /e/OS by removing a non Android OS

If you already have a non android OS like UbuntuTouch on your GS290, follow the instructions given [here](../../pages/install_e_GS290_linux) before proceeding with the rest of the installation

## Ensure you are on Android 10

Before installing /e/ on the GS290, kindly be sure it is on Android 10 stock ROM.
Info is visible in <code>Settings</code>, and update should be available OTA if it is still on Android 9.

## Before flashing

1.&nbsp;Check fastboot version
   <br>
   <br>
   <code>fastboot --version</code>

   <code>fastboot version 30.0.5-6877874</code>

{% include alerts/danger.html content="Versions provided by Ubuntu or some operating system are not up to date, do not try to flash your Gigaset with it. Instead, follow the instructions given [here](/pages/install-adb)" %}

2.&nbsp; Enable developer mode and following the instructions given [here](/pages/enable-usb-debugging)


3.&nbsp;Unlock your device
{% include alerts/danger.html content="Unlocking the bootloader will wipe out all your data. Ensure you have taken a backup as mentioned in the Requirements section above." %}

   ```
   adb reboot bootloader
   fastboot flashing unlock
   ```
4.&nbsp;On your phone screen confirm by pressing volume up
