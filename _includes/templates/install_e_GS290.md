## Install /e/

1. Download and extract the /e/ ROM 
1. Download `boot.img` and `recovery.img` next to `system.img` file
1. Reboot in bootloader with `adb reboot bootloader`
1. Press <kbd>volume up</kbd>  on the smartphone 
1. Run the following commands to install /e/

  ```  
  fastboot flash --disable-verity --disable-verification boot boot.img
  fastboot flash recovery recovery.img
  fastboot flash system system.img
  fastboot -w
  fastboot reboot
  ```
