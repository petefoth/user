## Unlocking the bootloader

{% include alerts/warning_unlocking_bootloader.md %}

1.&nbsp;Enable OEM unlock in the Developer options under device Settings, if present.

2.&nbsp;Connect the device to your PC via USB.

3.&nbsp;On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

   ```shell
   adb reboot bootloader
   ```
{% include alerts/tip.html content="You can also boot into fastboot mode via a key combination "%}
   - {{ device.download_boot}}

4.&nbsp;Once the device is in fastboot mode, verify your PC finds it by typing:

   ```shell
   fastboot devices
   ```
5.&nbsp;Now type the following command to obtain your bootloader unlock token:

   ```shell
   fastboot oem get_identifier_token
   ```
6.&nbsp;Visit the [HTCDev Bootloader Unlock](http://www.htcdev.com/bootloader/) website and follow the instructions there to obtain your unlock key and unlock your bootloader. If your device does not appear in the drop-down list, select All Other Supported Models.

7.&nbsp;If the device doesn’t automatically reboot, reboot it. It should now be unlocked.

8.&nbsp;Since the device resets completely, you will need to re-enable USB debugging to continue.

{% if device.is_ab_device %}
{% include templates/recovery_install_fastboot_ab.md %}
{% else %}
{% include templates/recovery_install_fastboot_generic.md %}
{% endif %}
