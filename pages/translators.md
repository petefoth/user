---
layout: page
title: Translators @/e/
permalink: translators
search: exclude
---

## Become a Translator (work remotely):

 - Would you love to read this website or our newsletter in your language?
 - Look no further. Join our international translation team.

## How to apply to be a Translator?

Please send an email to <contact@e.email> with your details and we will get back to you.

## Help translate the text in /e/OS

 - Anyone with an [/e/Gitlab ID](https://gitlab.e.foundation) can help translate the text in the /e/OS in their own language
 - We use Weblate to translate the text across Applications used on the /e/OS.
 - To understand how the /e/ Weblate Instance works [click here](how-to-use-weblate)
