---
layout: page
title: Try the Easy Installer beta
permalink: easy-installer
toc: true
---

## What is the /e/OS easy-installer?

- The Easy Installer is a desktop application which **helps users install /e/** on supported devices.

- In its current version the tool works on the **Linux** and **Windows OS**

![](/images/easy-installer-mainscreen.png)

## List of devices supported by the easy-installer

- The easy-installer beta version supports **9 devices**
  - Gigaset GS290
  - Fairphone 3
  - Fairphone 3+
  - Samsung Galaxy S9 – Exynos only
  - Samsung Galaxy S9 Plus – Exynos only
  - Samsung Galaxy S8 – Exynos only
  - Samsung Galaxy S8 Plus – Exynos only
  - Samsung Galaxy S7 – Exynos only
  - Samsung Galaxy S7 Edge – Exynos only
  

## Why are more devices and OS not supported?

The /e/ team cannot support all the work needed to add more devices or other operating systems. That's why we ask the community to help us on:
- Porting the Easy Installer to MacOS
   - the java software should run without big issues
   - but script need to be ported
- Support new devices
   - with its architecture based configuration files, it is easy to add new devices
   - we share details on how you can add more devices in [this document](easy-installer-contribute)

## How can I install the /e/OS easy-installer?

The easy-installer beta is available for Linux and Windows OS.

[Installation guide for Linux](easy-installer-linux)

[Installation guide for Windows](easy-installer-windows)


## I want to develop the source further. How can I help?

To build or improve upon the easy-installer code check the [documentation here...](easy-installer-contribute)


{% include alerts/tip.html content="To discuss about the easy-installer project, get help or contribute on it come to [our Community Forum](https://community.e.foundation/c/community/easy-installer/136)" %}

**Thank you in advance!**
