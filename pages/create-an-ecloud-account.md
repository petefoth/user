---
layout: page
title: Create a free /e/ account
permalink: create-an-ecloud-account
search: exclude
toc: true
---

### Advantages of an /e/ account

* having a new brand email address @e.email
* having a single identity to access all /e/ services :

   - /e/ Email : username@e.email
   - /e/ Drive : photos, videos, documents...
   - /e/ Calendar
   - /e/ Contacts
   - /e/ Notes
   - /e/ Tasks

* in your /e/ smartphone and online at : [https://ecloud.global](https://ecloud.global)

### How to get your free /e/ account ?

* Go to [https://e.foundation/e-mail-invite/](https://e.foundation/e-mail-invite/) you should see a screen like this

![](images/e-email.png)

* And follow the below steps :
	 - enter your current email address
	 - wait for an invitation in your mailbox ! Usually it takes < 2 minutes. Check your SPAM box !

### Get Started

* After you clicked the link in the invitation email you can proceed to the actual account creation :

![](images/create-e-account.png)

### Creating your own /e/ account @e.email

* Fill the fields correctly! This example will create an /e/ identity as John Doe <john.doe@e.email>
* Your username may contain Latin letters \(a-z\), digits \(0-9\) and special characters (\- . \_)
* Your username must be at least 3 characters long and at most 30 characters long
* Your username must not contain special characters in succession
* Your username must not start or end with special characters
* Please note that your username will be automatically converted to lowercase during account creation \(i.e. "John.Doe@e.email" becomes "john.doe@e.email"\)

![](images/your-e-id.png)

### Using an /e/ account @e.email

* Now that your /e/ account is created, you can use it in your /e/ smartphone 
  - In the first time usage wizard just after /e/ OS installation
  - You can add account by going to..
     ````
     Settings >> Accounts >> Add account >> /e/
     ````

* On the web at [https://ecloud.global](https://ecloud.global) to retrieve all the pictures, videos you took with your /e/ smartphone
* To access on the [ecloud](https://ecloud.global) all you emails and check your calendar
* 

### Having trouble creating an /e/ account @e.email?

* If you never received the invitation on your current email, check your email client SPAM folders
* If you cannot login in your /e/ smartphone or at [https://ecloud.global](https://ecloud.global) try to reset your password with the reset link available at [https://ecloud.global](https://ecloud.global)
* Make sure that you use your username@e.email as email for the reset process, NOT your recovery email.
* If that does not work for you contact us at <contact@e.email>

### How can I delete my /e/ account @e.email?

* Log in to your [ecloud account](https://ecloud.global/) using the ID you want to delete
* Take a backup of your data and emails (if required) before proceeding
* Click on your profile image
* This will open up a menu
* Click on Settings
* This will display the Setting options as shown in the screenshot below

![](images/Account_delete.png)

* Click on "Delete Account"
* This will open up a screen with the text "Check this to confirm the deletion request"
* Select the check box
* Click "Delete my Account" button
* The operation could take up to a minute. Please wait
  >  This action will remove your /e/ ID and ecloud account, data stored on the ecloud if any and all emails.

* You will be logged out of [ecloud.global](https://ecloud.global/)
* The /e/ account deletion is complete
> In case of any issues please send in the details to <support@e.email>

### Some more tips on your /e/ account @e.email

* Your /e/ identity at ecloud.global is not (yet) compatible with our various websites and forums.

* This means that you will have to create specific login/password at [community.e.foundation](https://community.e.foundation/), [gitlab.e.foundation](https://gitlab.e.foundation/e), and [e.foundation (shop)](https://e.foundation/e-pre-installed-refurbished-smartphones/)
* Do not try to login in your /e/ smartphone with your community or gitlab identity. It will not work.
* Your /e/ identity is under this form :
  ***username@e.email***

* This is your email address too. Yes. « .email » is a valid extension on internet, like .com, .org, .xyz and thousands others.

* And forget username@e.email.org we don't own the email.org domain.

### Want to manually configure your mail client for /e/?

Most mail clients should be able to configure your /e/ ID automatically, once you input your /e/ username and password. In case that is not possible, please use these settings to manually configure:

SMTP/IMAP server: mail.ecloud.global

SMTP/IMAP username: your /e/ email address in full (example: john.doe@e.email)

SMTP/IMAP password: your /e/ email password

SMTP port: 587, plain password, STARTTLS, requires authentication

IMAP port: 993, SSL/TLS, requires authentication


### Storage Options
* Your free account will be **limited to 1GB** online storage. 

* We now have some premium storage plans available. You can also **upgrade it to 20GB** while supporting the /e/ project by becoming an [/e/ Early Adopter](https://e.foundation/donate/).

**PLEASE KEEP IN MIND** that /e/ Free drive and mail accounts are supported by donations! 

Please [support us now and receive a gift](https://e.foundation/support-us/).

### Is an /e/ account mandatory to use /e/OS ?
* An /e/ account at ecloud.global is not mandatory to use /e/OS, but is highly recommended if you want to benefit from the full /e/ experience as described in this guide. 



