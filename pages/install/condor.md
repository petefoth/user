---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/condor/install
device: condor
---
{% include templates/device_install.md %}
