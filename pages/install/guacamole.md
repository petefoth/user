---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/guacamole/install
device: guacamole
---
{% include templates/device_install.md %}
