---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/harpia/install
device: harpia
---
{% include templates/device_install.md %}
