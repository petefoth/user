---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/sailfish/install
device: sailfish
---
{% include templates/device_install.md %}
