---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/bardock/install
device: bardock
---
{% include templates/device_install.md %}