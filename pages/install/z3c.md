---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/z3c/install
device: z3c
---
{% include templates/device_install.md %}