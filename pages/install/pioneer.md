---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/pioneer/install
device: pioneer
---
{% include templates/device_install.md %}
