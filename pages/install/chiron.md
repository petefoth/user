---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/chiron/install
device: chiron
---
{% include templates/device_install.md %}
