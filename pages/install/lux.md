---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/lux/install
device: lux
---
{% include templates/device_install.md %}
