---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/scorpio/install
device: scorpio
---
{% include templates/device_install.md %}