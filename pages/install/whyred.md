---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/whyred/install
device: whyred
---
{% include templates/device_install.md %}
