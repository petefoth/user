---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/albus/install
device: albus
---
{% include templates/device_install.md %}
