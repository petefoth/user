---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/lithium/install
device: lithium
---
{% include templates/device_install.md %}