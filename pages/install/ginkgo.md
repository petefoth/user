---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/ginkgo/install
device: ginkgo
---
{% include templates/device_install.md %}