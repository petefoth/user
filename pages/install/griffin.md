---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/griffin/install
device: griffin
---
{% include templates/device_install.md %}