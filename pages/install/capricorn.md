---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/capricorn/install
device: capricorn
---
{% include templates/device_install.md %}
