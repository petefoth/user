---
sidebar: home_sidebar
title: Install /e/ on Olimex Laptops
folder: install
layout: default
permalink: /devices/olimex/install
device: olimex
---
{% include templates/laptop_install.md %}
