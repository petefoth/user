---
sidebar: home_sidebar
title: Install /e/ on FairPhone FP3 - FP3
folder: install
layout: default
permalink: /devices/FP3/install
device: FP3
---
{% include templates/device_install.md %}
