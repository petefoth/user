---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/falcon/install
device: falcon
---
{% include templates/device_install.md %}
