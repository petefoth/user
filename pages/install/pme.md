---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/pme/install
device: pme
---
{% include templates/device_install.md %}
