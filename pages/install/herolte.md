---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/herolte/install
device: herolte
---
{% include templates/device_install.md %}
