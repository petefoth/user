---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/sagit/install
device: sagit
---
{% include templates/device_install.md %}
