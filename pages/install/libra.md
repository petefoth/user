---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/libra/install
device: libra
---
{% include templates/device_install.md %}
