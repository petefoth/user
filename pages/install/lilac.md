---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/lilac/install
device: lilac
---
{% include templates/device_install.md %}