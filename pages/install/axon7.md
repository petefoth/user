---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/axon7/install
device: axon7
---
{% include templates/device_install.md %}
