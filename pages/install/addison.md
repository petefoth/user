---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/addison/install
device: addison
---
{% include templates/device_install.md %}
