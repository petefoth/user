---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/land/install
device: land
---
{% include templates/device_install.md %}
