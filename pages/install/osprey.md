---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/osprey/install
device: osprey
---
{% include templates/device_install.md %}
