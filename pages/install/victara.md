---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/victara/install
device: victara
---
{% include templates/device_install.md %}