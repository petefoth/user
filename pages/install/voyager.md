---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/voyager/install
device: voyager
---
{% include templates/device_install.md %}