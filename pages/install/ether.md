---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/ether/install
device: ether
---
{% include templates/device_install.md %}