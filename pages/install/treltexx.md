---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/treltexx/install
device: treltexx
---
{% include templates/device_install.md %}
