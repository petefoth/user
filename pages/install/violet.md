---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/violet/install
device: violet
---
{% include templates/device_install.md %}
