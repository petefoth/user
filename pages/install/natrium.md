---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/natrium/install
device: natrium
---
{% include templates/device_install.md %}
