---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/nash/install
device: nash
---
{% include templates/device_install.md %}