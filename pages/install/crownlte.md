---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/crownlte/install
device: crownlte
---
{% include templates/device_install.md %}