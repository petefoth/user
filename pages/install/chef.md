---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/chef/install
device: chef
---
{% include templates/device_install.md %}