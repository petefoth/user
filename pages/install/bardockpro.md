---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/bardockpro/install
device: bardockpro
---
{% include templates/device_install.md %}