---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/twolip/install
device: twolip
---
{% include templates/device_install.md %}