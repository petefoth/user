---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/merlin/install
device: merlin
---
{% include templates/device_install.md %}