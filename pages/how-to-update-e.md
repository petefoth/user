---
layout: page
title: How to update /e/
permalink: how-to-update-e
---

1. Open the updater app

    `Settings` > `System` > `Advanced` > `Updater`

    ![](images/screenshot_20201119_10:38:46.png)
    ![](images/screenshot_20201119_10:38:53.png)
    ![](images/screenshot_20201119_10:39:01.png)
    ![](images/screenshot_20201119_10:39:06.png)

1. On the last available update, tap on `Download`

    ![](images/screenshot_20201119_10:39:11.png)

    > If no update available, tap the **refresh button** on top right

    > The Updater download the necessary file. If you are on LTE, a warning popup ask you if you prefer to switch to Wifi

1. Once downloaded, tap on `Install`. Tap on `OK` to start the installation.

    ![](images/screenshot_20201119_10:54:46.png)
    ![](images/screenshot_20201119_10:56:03.png)

    > The Install process starts. It may take a long time

1. Once finished, reboot your device.

    ![](images/screenshot_20201119_11:08:39.png)

The device reboots. Update applied! 👏
