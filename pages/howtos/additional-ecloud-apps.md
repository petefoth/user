---
layout: page
title: Enable additional apps on eCloud
permalink: /how-tos/additional-ecloud-apps
toc: true
---

## Purpose of the document

This guide explains how to control which applications are available on your ecloud.global account, disabling those that you don't need and enabling those that provide functionality which is of interest to you.
It also tries to cover some of the frequently asked question around app availability on ecloud.global.

## FAQ on available applications

### Which applications are currently available on ecloud.global?
   - Files
   - E-mail (Rainloop)
   - Contacts
   - Calendar
   - Photos
   - Notes (Nextcloud)
   - Tasks
   - Activity
   - Carnet (alternative notes app)
   - Deck (project planning board)
   - Bookmarks
   - News (RSS reader)
   
### Why don't you offer all possible applications?
For different reasons:
 - All need checking and sometimes tweaking. Some don't work out of the box or require external components.
 - Once we enable them, we commit to the app being available in the future, keeping it updated and offering support to our users.
 - We need to consider security and performance impact of a given app if enabled for everyone.
 - It would complicate the UI and usability of the cloud.

We [ran a survey](https://community.e.foundation/t/adding-new-apps-at-ecloud-global/12404/69) last year and we used the results to enable _Carnet_, _Deck_, _Bookmarks_ and _News_.<br>
_Maps_ and _Forms_ need further polish before they can be made available.
 
### Can you enable application XYZ?

We will run similar polls from time to time. You can create a topic on the [Community Forum](https://community.e.foundation/) and see if other users agree with such app being useful. We will then notice popular topics and evaluate the proposal. 
 
### Do premium subscribers have access to more or better apps?

Not for now, but this may change in the future. For instance, we may offer apps that have some higher hardware requirements (like streaming music), or a licensed limit on the number of users.

## Customizing applications available on your account

1. Log into ecloud.global with your username and password.
2. Click on the image or initials on the top right corner, then _Settings_.
3. On the left sidebar there will be a new menu, click on _App order_. Or visit [this link](https://ecloud.global/settings/user/apporder). 
4. With the blue ticks you can enable or disable applications which are available to you.<br>Changes take effect immediately after each click, but you need to reload the page to see them reflected in the top navigation bar. 

![Image](/images/howtos/ecloud/choosing_applications.png)


## Configuring application order

In the same screen as the previous point, you can decide the order of the applications in the navigation.

<video controls="controls" width="800" height="600" class="mw-100"
       name="Reordering applications" poster="/images/howtos/ecloud/reordering_applications.png">
       	<source src="/images/howtos/ecloud/reordering_applications.mov" type="video/mp4">
       	<source src="/images/howtos/ecloud/reordering_applications.webm" type="video/webm">
</video>

