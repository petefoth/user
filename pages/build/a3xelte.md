---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy A3 (2016 Exynos) - a3xelte
folder: build
layout: page
permalink: /devices/a3xelte/build
device: a3xelte
---
{% include templates/device_build.md %}
