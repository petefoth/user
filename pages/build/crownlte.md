---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Mi Note 2 - scorpio
folder: build
layout: page
permalink: /devices/scorpio/build
device: scorpio
---
{% include templates/device_build.md %}