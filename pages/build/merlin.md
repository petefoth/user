---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto G3 Turbo - merlin
folder: build
layout: page
permalink: /devices/merlin/build
device: merlin
---
{% include templates/device_build.md %}