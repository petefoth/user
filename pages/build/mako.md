---
sidebar: home_sidebar
title: Build /e/ for Google Nexus 4 - mako
folder: build
layout: page
permalink: /devices/mako/build
device: mako
---
{% include templates/device_build.md %}
