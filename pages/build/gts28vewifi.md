---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy Tab S2 8.0 Wi-Fi (2016) - gts28vewifi
folder: build
layout: page
permalink: /devices/gts28vewifi/build
device: gts28vewifi 
---
{% include templates/device_build.md %}