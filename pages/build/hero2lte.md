---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S7 edge - hero2lte
folder: build
layout: page
permalink: /devices/hero2lte/build
device: hero2lte
---
{% include templates/device_build.md %}
