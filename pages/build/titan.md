---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto G 2014 - titan
folder: build
layout: page
permalink: /devices/titan/build
device: titan
---
{% include templates/device_build.md %}
