---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S III Neo - s3ve3gds
folder: build
layout: page
permalink: /devices/s3ve3gds/build
device: s3ve3gds
---
{% include templates/device_build.md %}