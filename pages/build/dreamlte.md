---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S8 - dreamlte
folder: build
layout: page
permalink: /devices/dreamlte/build
device: dreamlte
---
{% include templates/device_build.md %}