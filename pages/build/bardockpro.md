---
sidebar: home_sidebar
title: Build /e/ for BQ Aquaris X Pro - bardockpro
folder: build
layout: page
permalink: /devices/bardockpro/build
device: bardockpro
---
{% include templates/device_build.md %}