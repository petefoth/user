---
sidebar: home_sidebar
title: Build /e/ for Sony XZ1 Compact - lilac
folder: build
layout: page
permalink: /devices/lilac/build
device: lilac
---
{% include templates/device_build.md %}