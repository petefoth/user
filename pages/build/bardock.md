---
sidebar: home_sidebar
title: Build /e/ for BQ Aquaris X - bardock
folder: build
layout: page
permalink: /devices/bardock/build
device: bardock
---
{% include templates/device_build.md %}