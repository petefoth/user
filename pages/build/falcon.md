---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto G - falcon
folder: build
layout: page
permalink: /devices/falcon/build
device: falcon
---
{% include templates/device_build.md %}
