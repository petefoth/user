---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy J7 (Exynos) - j7elte
folder: build
layout: page
permalink: /devices/j7elte/build
device: j7elte
---
{% include templates/device_build.md %}
