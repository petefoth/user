---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto Z2 Play - albus
folder: build
layout: page
permalink: /devices/albus/build
device: albus
---
{% include templates/device_build.md %}
