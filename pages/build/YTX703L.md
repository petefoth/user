---
sidebar: home_sidebar
title: Build /e/ for Lenovo Yoga Tab 3 Plus - YTX703L
folder: build
layout: page
permalink: /devices/YTX703L/build
device: YTX703L
---
{% include templates/device_build.md %}