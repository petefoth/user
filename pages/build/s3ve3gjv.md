---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S III Neo - s3ve3gjv
folder: build
layout: page
permalink: /devices/s3ve3gjv/build
device: s3ve3gjv
---
{% include templates/device_build.md %}