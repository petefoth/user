---
sidebar: home_sidebar
title: Build /e/ for HTC One (M8) Dual Sim - m8d
folder: build
layout: page
permalink: /devices/m8d/build
device: m8d
---
{% include templates/device_build.md %}