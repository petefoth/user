---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S8+ - dream2lte
folder: build
layout: page
permalink: /devices/dream2lte/build
device: dream2lte
---
{% include templates/device_build.md %}