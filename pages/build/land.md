---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Redmi 3S/3X - land
folder: build
layout: page
permalink: /devices/land/build
device: land
---
{% include templates/device_build.md %}
