---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy J5N LTE - j5nlte
folder: build
layout: page
permalink: /devices/j5nlte/build
device: j5nlte
---
{% include templates/device_build.md %}
