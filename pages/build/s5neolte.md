---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S5 Neo - s5neolte
folder: build
layout: page
permalink: /devices/s5neolte/build
device: s5neolte
---
{% include templates/device_build.md %}
