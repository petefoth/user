---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S III (International) - i9300
folder: build
layout: page
permalink: /devices/i9300/build
device: i9300
---
{% include templates/device_build.md %}
