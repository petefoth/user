---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto Z2 Force - nash
folder: build
layout: page
permalink: /devices/nash/build
device: nash
---
{% include templates/device_build.md %}