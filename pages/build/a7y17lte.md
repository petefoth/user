---
sidebar: home_sidebar
title: Build /e/ for Samsung Samsung Galaxy A7 (2017) - a7y17lte
folder: build
layout: page
permalink: /devices/a7y17lte/build
device: a7y17lte
---
{% include templates/device_build.md %}
