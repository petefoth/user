---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy Tab S2 9.7 Wi-Fi (2016) - gts210vewifi
folder: build
layout: page
permalink: /devices/gts210vewifi/build
device: gts210vewifi 
---
{% include templates/device_build.md %}