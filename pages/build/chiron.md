---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Mi MIX 2 - chiron
folder: build
layout: page
permalink: /devices/chiron/build
device: chiron
---
{% include templates/device_build.md %}
