---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Mi MIX - lithium
folder: build
layout: page
permalink: /devices/lithium/build
device: lithium
---
{% include templates/device_build.md %}