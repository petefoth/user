---
sidebar: home_sidebar
title: Build /e/ for FairPhone FP3 - FP3
folder: build
layout: default
permalink: /devices/FP3/build
device: FP3
---
{% include templates/device_build.md %}
