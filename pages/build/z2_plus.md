---
sidebar: home_sidebar
title: Build /e/ for ZUK Z2 Plus - z2_plus
folder: build
layout: page
permalink: /devices/z2_plus/build
device: z2_plus
---
{% include templates/device_build.md %}