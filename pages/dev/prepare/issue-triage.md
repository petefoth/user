---
layout: page
title: Issue triage
permalink: internal/development-process/prepare/issue-triage
search: exclude
---

## What ?

This article speak about the way to handle all the issues, and more specificaly those opened by the community.

## Why ?

The aim is to be able to facilitate the priorisation, by highlighting bugs. The role is also to clean the issues, by following a template and closing duplicates.

## Who ?

| People | Role |
| ------ | ---- |
|        |      |


## How ?

Take issues from [here](https://gitlab.e.foundation/e/management/issues) one by one (in priority those that don't have any label) and follow the following process :

1.  Validate the issue
   - if it's a duplication, just mark is a duplicate with a reference to the original one. Please prefer to keep the oldest one
   - if it's a bug, try to replicate it and ask the author if needed more details
   - if it's a feature or an improvement, validate it does not already exist or that it is in the scope of the project
1.  Prepare the issue
   - reword the title if needed
   - apply an issue template if not already done
   - reword some part of the description if needed
1. Qualify the issue
   - type: feature, bug, improvement, task/backstage, epic
   - device specific: device specific, \<device codename\>
   - android version: nougat, oreo, pie
   - scope: os, launcher, mail
   - ~~facet: documentation, infra, dev, UI, hardware~~
1.  Set a weight to the issue
   - define the weight of an issue, between 1 and 10
   - weight speak about the complexity of a task
   - the weight is abstract (not related directly to any time)
      - 1: trivial
      - 2
      - 3
      - 4
      - 5: very hard or complex, need to be split

## Where ?

GitLab /e/ project issues: https://gitlab.e.foundation/e/management/issues

## When ?

Once a day

---

[Next (Priorisation) >](priorisation)
