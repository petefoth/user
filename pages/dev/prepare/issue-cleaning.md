---
layout: page
title: Issue cleaning
permalink: internal/development-process/prepare/issue-cleaning
search: exclude
---


## What ?



## Why ?

## Who ?

|      |
| ---- |
|      |

## How ?

## Where ?

## When ?

After each important release

---

[Next (Issue assignment) >](../dev/issue-assignment)
