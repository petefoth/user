---
layout: page
title: Planification
permalink: internal/development-process/prepare/planification
search: exclude
---


## What ?

Give visibility on the timing to solve an issue

## Why ?

For users, to know when the issue can be solved.

For the dev team, to give visibility on our goals.

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

**Long term**

We use a label for each major version.
  - v::x.y.z

In case of an emergency, we can use the v::emergency to alert that the bug have
to be solved ASAP, out of the normal dev process.

**Medium term**

We use the milestone gitlab feature. -> milestone
  - assign an issue to a milestone

**Short term**

We use the status:To Do label and the order in the board list to prioritize dev issue. This topic in cover in []().

## Where ?

## When ?

Except the v::emergency that is given during th issue-triage step, everything is done once a week.

---

[Next (sprint) >](sprint)
