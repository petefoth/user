---
layout: page
title: Priorisation
permalink: internal/development-process/prepare/priorisation
search: exclude
---

## What ?

Give a priority to each issue

## Why ?

The aim is to handle the issues that are the most important, at the same time from a project, community and user point of view.

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

We can here make a distinguish between bugs issues and others.

### Bug issues

#### Impact

The impact represent who is affected by the bug. The number or the profile (premium) of users impacted must be taken in account.

| ~      | impact        | Definition                                                   |
| ------ | ------------- | ------------------------------------------------------------ |
| **I1** | High impact   | Almost all users are concerned, a majority of premium users, big impact on the project image |
| **I2** | Large impact  | Lot of users are concerned, some premium users , impact on the project image |
| **I3** | Medium impact | Only few users are concerned                                 |
| **I4** | Low impact    | The bug affected only some users in a particular case        |

#### Severity

The severity represent the influence of the bug from the user point of view.

| ~      | severity          | Definition                                                   |
| ------ | ----------------- | ------------------------------------------------------------ |
| **S1** | Blocker           | The user cannot use his/her device: it does not boot, it crashes, network is not working |
| **S2** | Critical severity | The user cannot use attendees features of the device, as Phone, Message, Mail, Calendar, Web browser, but also main applications of the market |
| **S3** | Major severity    | The user cannot use some features or apps on his/her smartphone |
| **S4** | Low severity      | Cosmetic issues                                              |

reference: https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#severity-labels

#### Priority

Define the priority is a combination of impact and severity.

| Impact\Severity | I1   | I2   | I3   | I4   |
| --------------- | ---- | ---- | ---- | ---- |
| S1              | P1   | P2   | P3   | P4   |
| S2              | P2   | P3   | P4   | P5   |
| S3              | P3   | P4   | P5   | P6   |
| S4              | P4   | P5   | P6   | P7   |

### Device specific issues

It's possible to interpret the impact as the popularity of the device, based both on:
- the number of users
- the /e/ support (/dev, /stable)

### Other issues

We can define the priority from scratch (P1 -> P7).

## Where ?

GitLab, priorisation board

- /e/ https://gitlab.e.foundation/groups/e/-/boards/296
- e/apps https://gitlab.e.foundation/groups/e/apps/-/boards/286
- e/os https://gitlab.e.foundation/groups/e/os/-/boards/289
- e/management https://gitlab.e.foundation/e/project/-/boards/292
- any other project

## When ?

Once a week, ideally in the same time or just after [issue triage](issue triage).

---

[Next (planification) >](planification)
