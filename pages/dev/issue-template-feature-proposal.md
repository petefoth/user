## Summary

<Summarize the feature briefly and precisely>

**This feature concerns** <Tick at least one of the following choices>
- [ ] UI <Something new in the UI>
- [ ] Behavior <A new behavior to realise something>
- [ ] Privacy <Something that will improve the privacy>

## Description

**Who will use the new feature?**

<Describe the kind of user that want this feature>

**What is the target of the new feature for this user?**

<Describe the result of the feature for the user>

**Why this user would like to use this feature?**

<Describe the reason the user would like this feature>

## Examples

<Give the example of what users will be able to accomplish with the feature>

## Reflection

**Mockups**

**Diagrams**

## Validation

<List test case that will be run to validate that the issue is working as expected>

/label ~"Feature Proposal"
