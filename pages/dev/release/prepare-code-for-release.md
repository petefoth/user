---
layout: page
title: Prepare code for release
permalink: internal/development-process/release/prepare-code-for-release
search: exclude
---

## What ?



## Why ?

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

1. Take the list of release MR
  - Go to https://gitlab.e.foundation/groups/e/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened
  - Add a filter on the milestone name
1. For each MR, confirm it working as expected by removing the *WIP:* prefix in the title
1. Merge it

For all the MR under e/apps group:
1. Once the MR is validated, a Pipeline will start automatically
1. Once the Pipeline succeed, pick up the artifact to add it onto the android_prebuilt_prebuiltapk MR

## Where ?

List of MR for a sprint

## When ?

Once the test session take end, and all the problem were solved or delayed

---

[Next (Release note) >](release-note)
