---
layout: page
title: Closing the sprint
permalink: internal/development-process/release/closing-the-sprint
search: exclude
---


## What ?

Replane issues that was not close

## Why ?

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?


## Where ?

## When ?


---

[Next (Issue assignment) >](../dev/issue-assignment)
