- /e/ version:
- Device model:
- Reproducible with the last /e/ version:
- Reproducible with LineageOS:

## Summary

<Summarize the improvement briefly and precisely>

**This improvement concerns** <Tick at least one of the following choices>
- [ ] UI
- [ ] Behavior
- [ ] Privacy

## Description

**What is the current behavior?**

<What actually happens>

**What is the improved behavior?**

<What you should see instead>

**What does it bring?**

<Why this improvement is needed>

## Examples

<Give the example of what users will be able to accomplish with the improvement>

## Validation

<List test case that will be run to validate that the issue is working as expected>

/label ~Improvement
