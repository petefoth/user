---
layout: page
title: Run test build
permalink: internal/development-process/test/run-test-build
search: exclude
---

## What ?

Run a build for test

## Why ?

Have a build including all the modification done on a sprint.

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

1. One each os project, place the code you want to test on a branch names **sprint_\<sprint name\>**
   - if you have a branch for each android version, please pre-fix your branches with **v1-nougat**, **v1-oreo** or **v1-pie**
2. Set the correct branch name on the test build scheduled https://gitlab.e.foundation/e/priv/os/build/pipeline_schedules/113/edit
3. Start manually the test build
4. Ensure that all the branch are correctly selected, by looking into the logs on the first devices builds

## Where ?

scheduled build: https://gitlab.e.foundation/e/priv/os/build/pipeline_schedules/113/edit

the pipeline

## When ?

At the end of a sprint

---

[Next (Test session) >](test-session)
