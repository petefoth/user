---
layout: page
title: Test session
permalink: internal/development-process/test/test-session
search: exclude
---

## What ?

Test the ROM before a release

## Why ?

the aim is to validate that the work done during the sprint is working as expected, both on a fresh install and on an update.

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

1. Share the test plan to the tester team ([here](https://office.ecloud.global/products/files/doceditor.aspx?fileid=2030))
1. Create a folder [here](https://office.ecloud.global/products/projects/tmdocs.aspx?prjID=38#341) to collect the test results
1. Share the test builds to the test team
1. Each tester run the test plan, and send the result into the folder created at point 2
1. Analyse the test result, and update each issue consequently
   - if the feature is not working as expected, or the problem is not fixed, kindly add a comment on the issue. It will have to be reopened
   - if you found a new problem, please open a new issue
       - if you think it is a blocking problem, please alert the eFoundation team immediately
   - if everything is correct, it's possible to congrats the dev team
1. All the test team have to be thanks for the work done on the test session
   - maybe a sumup of what have been done could be helpful

## Where ?

The OS image is available on gitlab

The sprint issue list is available

## When ?

Once the test build is finished

---

[Next (Prepare code for release) >](../release/prepare-code-for-release)
