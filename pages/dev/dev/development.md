---
layout: page
title: Development
permalink: internal/development-process/dev/development
search: exclude
---

## What ?



## Why ?



## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

1. The developer works on the issue he is assigned to
2. He starts by creating the MR related to the issue, on the corresponding project. This creation have to be done from the GitLab issue UI. It will give the branch name to use. The MR title have to be pre-fixed by WIP, and the MR template must be filled. The MR is assigned to the developer.
    - the target branch of the MR should be
      - the dev branch if the project has one
      - the sprint branch in other cases
3. The developper starts the dev. For development specification, please refer to each project documentation
4. Once finished or at least once a day for backup reason, the code is pushed to the gitlab branch
5. The apk build by GitLab CI have to be tested, by respecting what is written in the MR description. If the test need to run a complete build, please run a test build, both for you and for the approver(s).
6. If everything is working as expected, remove the WIP status and ask another sprint developer to approve the MR
7. Add test cases into the issue description, and mark is as status::done

The developer assigned to an issue is responsible of it's development.

## Where ?

The GitLab issue

## When ?

Once the developer is assigned to an issue.

---

[Next (Validation) >](validation)
