---
layout: page
title: How Tos
permalink: how-tos/
search: exclude
toc: true
---
You can find links to the complete set of HOWTO's available [on the /e/ community forums](https://community.e.foundation/t/list-of-howtos-add-your-suggestions/12658#heading--e-installation).

## Installation

- [[HOWTO] Buy a new phone with /e/ already installed on it!](https://e.foundation/e-pre-installed-refurbished-smartphones/)
- [[HOWTO] Find out if my device is supported by /e/](https://community.e.foundation/t/howto-find-out-if-my-device-is-supported-by-e/2889)
- [[HOWTO] Find if there is a Nougat , Oreo or Pie build for my device](https://community.e.foundation/t/howto-find-if-there-is-a-nougat-oreo-or-pie-build-for-my-device/2891)
- [[HOWTO] What is ADB? (Android Debug Bridge)](https://community.e.foundation/t/howto-what-is-adb-android-debug-bridge/3057)
- [[HOWTO] Install and Setup ADB and Fastboot](/pages/install-adb)
- [[HOWTO] Find device code](https://community.e.foundation/t/howto-find-device-code/3066)
- [[HOWTO] Enable USB Debugging](/pages/enable-usb-debugging)
- [[HOWTO] Install TWRP](https://community.e.foundation/t/howto-install-twrp/3056)
- [[HOWTO] Backup and restore with TWRP](/pages/backup-and-restore-with-twrp)
- [[HOWTO] Enable developer options and ADB/USB debugging](https://community.e.foundation/t/howto-enable-developer-options-and-adb-usb-debugging/3055)
- [[HOWTO] Install /e/ on a Samsung smartphone with Windows easily](https://community.e.foundation/t/howto-install-e-on-a-samsung-smartphone-with-windows-easily/3186)
- [[HOWTO] Android bootloader, fastboot, recovery and normal booting](https://community.e.foundation/t/howto-android-bootloader-fastboot-recovery-and-normal-booting/3052)
- [[HOWTO] Unlock bootloader](https://community.e.foundation/t/howto-unlock-bootloader/3053)
- [[HOWTO] Install Heimdall](https://doc.e.foundation/how-tos/install-heimdall)
- [[HOWTO] Build Heimdall](https://doc.e.foundation/how-tos/build-heimdall)
- [[HOWTO] Update /e/](https://community.e.foundation/t/howto-update-e/3222)
- [Update /e/OS software](https://community.e.foundation/t/update-e-os-software/13411)
- [Updating to /e/OS Pie using fastboot, adb, and TWRP](https://community.e.foundation/t/updating-to-e-os-pie-using-fastboot-adb-and-twrp/12770)
- [[HOWTO] Factory reset my device](https://doc.e.foundation/how-tos/factory-reset)
- [Reset your /e/phone to Factory Settings](https://community.e.foundation/t/reset-your-e-phone-to-factory-settings/13414)
- [[HOWTO] Upgrade manually from e-Nougat, e-Oero to official e-Pie builds without data loss](https://community.e.foundation/t/howto-upgrade-manually-from-e-nougat-e-oero-to-official-e-pie-builds-without-data-loss/10254)

## Start-up and Set-up /e/

- [Changing Phone’s Ringtone to your favourite song or tune](https://community.e.foundation/t/changing-phones-ringtone-to-your-favourite-song-or-tune/13099)
- [Take, crop and share a screenshot](https://community.e.foundation/t/take-crop-and-share-a-screenshot/13104)
- [[HOWTO] Sync your contacts, calendar events, email, notes and tasks to /e/](https://community.e.foundation/t/howto-sync-your-contacts-calendar-events-email-notes-and-tasks-to-e/3263)
- [Disable /e/Cloud’s automatic sync of Contacts and/or Media (Photos and videos)](https://community.e.foundation/t/disable-e-clouds-automatic-sync-of-contacts-and-or-media-photos-and-videos/13150)
- [[HOWTO] Disable finger sensor on Redmi Note](https://community.e.foundation/t/howto-disable-finger-sensor-on-redmi-note-4/700)
- [Setup Screen pinning](https://community.e.foundation/t/setup-screen-pinning/13348)
- [Change the /e/OS interface language](https://community.e.foundation/t/change-the-e-os-interface-language/13109)
- [Transfer your data to-and-fro /e/phone via USB cable](https://community.e.foundation/t/transfer-your-data-to-and-fro-e-phone-via-usb-cable/13419)
- [Display modes & Health tips](https://community.e.foundation/t/display-modes-health-tips/13286)
- [Manage Dual SIMs](https://community.e.foundation/t/manage-dual-sims/13501)

## Applications and widgets in /e/

- [[HOWTO] Understand,configure and use the contact tracing framework in microG](/how-tos/microg-exposure-notification-version-implementation)
- [[HOWTO] Find free/open-source alternatives of your apps/services (editable)](https://community.e.foundation/t/howto-find-free-open-source-alternatives-of-your-apps-services/13616)
- [[HOWTO] Install F-Droid App](https://community.e.foundation/t/howto-install-f-droid-app/432)
- [Install an App from a 3rd party source or APK file directly](https://community.e.foundation/t/install-an-app-from-a-3rd-party-source-or-apk-file-directly/13341)
- [App permissions](https://community.e.foundation/t/app-permissions/13304)
- [Add, remove and configure Widgets](https://community.e.foundation/t/add-remove-and-configure-widgets/13425)
- [Third party launchers](https://community.e.foundation/t/third-party-launchers/13354)
- [[HOWTO] Use the AppStore](https://community.e.foundation/t/howto-use-the-appstore/4142)
- [[HOWTO] Using WhatsApp under shelter](https://community.e.foundation/t/howto-using-whatsapp-under-shelter/5662)
- [[HOWTO] Installing Banking Apps](https://community.e.foundation/t/howto-installing-banking-apps/5875)
- [Call forwarding, waiting and blocking](https://community.e.foundation/t/call-forwarding-waiting-and-blocking/13205)
- [Instant secure and private video/audio calls with integrated group/private chat using /e/visio via Jitsi Meet App](https://community.e.foundation/t/instant-secure-and-private-video-audio-calls-with-integrated-group-private-chat-using-e-visio-via-jitsi-meet-app/13268)
- [Clear an App’s cache data](https://community.e.foundation/t/clear-an-apps-cache-data/13363)
- [Set up an alternate email](https://community.e.foundation/t/set-up-an-alternate-email/13096)
- [[HOWTO] Add a gmail ID from the mail app](https://community.e.foundation/t/howto-add-a-gmail-id-from-the-mail-app/2807)
- [[HOWTO] Add a web site to the home screen](https://community.e.foundation/t/howto-add-a-web-site-to-the-home-screen/2090/2)
- [[HOWTO] Enable PGP/MIME encryption with K-9 Mail and OpenKeychain](https://community.e.foundation/t/howto-enable-pgp-mime-encryption-with-k-9-mail-and-openkeychain/2369)

## System apps

 - [[HOWTO] Use the AppStore (editable)](https://community.e.foundation/t/howto-use-the-appstore/4142)
 - [[HOWTO] Use the BlissLauncher (editable)](https://community.e.foundation/t/howto-use-the-blisslauncher/13438)
 - [[HOWTO] Use the Clock App (editable)](https://community.e.foundation/t/howto-use-the-clock-app/13446)
 - [[HOWTO] Use the Recorder App (editable)](https://community.e.foundation/t/howto-use-the-recorder-app/13456)
 - [[HOWTO] Use the Message App  (editable)](https://community.e.foundation/t/howto-use-the-message-app/13454)
 - [[HOWTO] Use the Maps app (editable)](https://community.e.foundation/t/howto-use-the-maps-app/13453)
 - [[HOWTO] Use the Mail App (editable)](https://community.e.foundation/t/howto-use-the-mail-app/13452)
 - [[HOWTO] Use the Gallery App (editable)](https://community.e.foundation/t/howto-use-the-gallery-app/13450)
 - [[HOWTO] Use the Keyboard (editable)](https://community.e.foundation/t/howto-use-the-keyboard/13451)
 - [[HOWTO] Use the Files App (editable)](https://community.e.foundation/t/howto-use-the-files-app/13449)
 - [[HOWTO] Use the Dailer App (editable)](https://community.e.foundation/t/howto-use-the-dailer-app/13448)
 - [[HOWTO] Use the Weather Widget (editable)](https://community.e.foundation/t/howto-use-the-weather-widget/13458)
 - [[HOWTO] Use the Calculator App (editable)](https://community.e.foundation/t/howto-use-the-calculator-app/13443)
 - [[HOWTO] Use the Contacts App (editable)](https://community.e.foundation/t/howto-use-the-contacts-app/13447)
 - [[HOWTO] Use the Camera App (editable)](https://community.e.foundation/t/howto-use-the-camera-app/13445)
 - [[HOWTO] Use the Calendar App (editable)](https://community.e.foundation/t/howto-use-the-calendar-app/13444)
 - [[HOWTO] Use the Tasks App (editable)](https://community.e.foundation/t/howto-use-the-tasks-app/13457)
 - [[HOWTO] Use the Notes App (editable)](https://community.e.foundation/t/howto-use-the-notes-app/13455)
 - [[HOWTO] Use the Browser App (editable)](https://community.e.foundation/t/howto-use-the-browser-app/13439)
 - [[HOWTO] Use the Account Manager (editable)](https://community.e.foundation/t/howto-use-the-account-manager/13437)

## eCloud, e.email & account

 - [[HOWTO] Create an /e/ account ](/create-an-ecloud-account)
 - [[HOWTO] Get an /e/ email ID](https://community.e.foundation/t/howto-get-an-e-email-id/2949)
 - [[HOWTO] Is my /e/ email ID a test ID / temporary ID?](https://community.e.foundation/t/howto-is-my-e-email-id-a-test-id-temporary-id/2952)
 
 - [[HOWTO] Use Thunderbird Contacts with /e/ Contacts (Should work for any OS)](https://community.e.foundation/t/howto-use-thunderbird-contacts-with-e-contacts-should-work-for-any-os/10798)
 - [[HOWTO] Setting up Lightning (Calendar) in Thunderbird for /e/ (Should work for any OS)](https://community.e.foundation/t/howto-setting-up-lightning-calendar-in-thunderbird-for-e-should-work-for-any-os/10923)
 - [[HowTo] Getting an /e/ email and Setting up Thunderbird (Should work for any OS)](https://community.e.foundation/t/howto-getting-an-e-email-and-setting-up-thunderbird-should-work-for-any-os/10949)
 - [[HOWTO] Email Configurations - to configure /e/ on most email clients](https://community.e.foundation/t/howto-email-configurations-to-configure-e-on-most-email-clients/632)
 - [[HOWTO] Access my /e/ email’s in Thunderbird](https://community.e.foundation/t/howto-access-my-e-emails-in-thunderbird/2962)
 - [[HOWTO] Access /e/-mailserver with Thunderbird](https://community.e.foundation/t/howto-access-e-mailserver-with-thunderbird/3154)
 - [[HOWTO] Sync the new eCloud calendars](/how-tos/how-to-sync-new-ecloud-calendars)
 - [[HOWTO] Sync contacts with webmail using Rainloop on eCloud](/how-tos/sync-contact-webmail)
 - [Install and connect Nextcloud app to your eCloud drive to unveil its true potential](https://community.e.foundation/t/install-and-connect-nextcloud-app-to-your-e-cloud-drive-to-unveil-its-true-potential/13218)
 - [[HOWTO] Enable additional apps on eCloud](/how-tos/additional-ecloud-apps)
 - [[HOWTO] Change your recovery email on ecloud.global](https://community.e.foundation/t/howto-e-cloud-global-change-your-recovery-email/10985/12)

## Migrate to /e/
### For iPhone Users
- [[HOWTO] Create an /e/ account ](/create-an-ecloud-account)
- [[HOWTO] Import contacts from Apple to eCloud](https://community.e.foundation/t/howto-import-contacts-from-apple-to-e-cloud/5532)
  
### Coming from Stock Android or Custom ROMs
- [[HOWTO] Create an /e/ account ](/create-an-ecloud-account)
- [[HOWTO] Import Gmail contacts into eCloud](https://community.e.foundation/t/howto-import-gmail-contacts-into-ecloud/2809)
- [[HOWTO] Import Google Keep notes into /e/](https://community.e.foundation/t/how-to-import-google-keep-notes-into-e/3165/3)
- [[HOWTO] Move contacts from Google account to /e/ (export, import)](https://community.e.foundation/t/howto-move-contacts-from-google-account-to-e-export-import/12390)
- [[HOWTO] Transfer WhatsApp messages from Google Android to /e/ (backup and restore)](https://community.e.foundation/t/howto-transfer-whatsapp-messages-from-google-android-to-e-backup-and-restore/12389)
- [[HOWTO] Download every app in the Google Play Store thanks to Aurora Store](https://community.e.foundation/t/howto-download-every-app-in-the-google-play-store-thanks-to-aurora-store/13622)

## The community forum , GitLab & WebLate
 - [[HOWTO] Get a “/e/ user” title on this forum](https://community.e.foundation/t/howto-get-a-e-user-title-on-this-forum/1024)
 - [[HOWTO] Report an issue ](report-an-issue)
 - [[HOWTO] Create a log ](https://doc.e.foundation/how-tos/create-a-log)
 - [[HOWTO] Get a log from your phone](https://community.e.foundation/t/howto-get-a-log-from-your-phone/2366)
 - [[HOWTO] Enable adb logcat on first boot without authentication (debugging boot loops)](https://community.e.foundation/t/howto-enable-adb-logcat-on-first-boot-without-authentication-debugging-boot-loops/4496)
 - [[HOWTO] Use Weblate - for translation team members](https://doc.e.foundation/how-to-use-weblate)
 

## TroubleShooting Issues

 - [Set up and troubleshoot a mobile data connection (3G/4G)](https://community.e.foundation/t/set-up-and-troubleshoot-a-mobile-data-connection-3g-4g/13155)
 - [[HOWTO] Phone not switching from 2G to higer - workaround](https://community.e.foundation/t/howto-phone-not-switching-from-2g-to-higer-workaround/646)
 - [[HOWTO] How to deal with unstable applications?](https://community.e.foundation/t/howto-how-to-deal-with-unstable-applications/3970)
 - [[HOWTO] Mail app sync issues - workaround](https://community.e.foundation/t/howto-mail-app-sync-issues-workaround/647)
 - [[HOWTO] Fix permissions error while syncing data](https://community.e.foundation/t/howto-fix-permissions-error-while-syncing-data/4508)
 - [[HOWTO] Essential Phone PH-1 Mata Eelo 0.7 Mobile Hotspot Tethering Fix](https://community.e.foundation/t/howto-essential-phone-ph-1-mata-eelo-0-7-mobile-hotspot-tethering-fix/6794)
 - [[HOWTO] Install /e/ on OnePlus 6 running Andoroid 10](https://community.e.foundation/t/howto-install-e-on-oneplus-6-running-andoroid-10/11134)

## For Advanced users
 - [[HOWTO] Capture network data and analyze with Wireshark](https://community.e.foundation/t/howto-capture-network-data-and-analyze-with-wireshark/10504)
 - [[HOWTO] Shut down apps properly](https://community.e.foundation/t/shut-down-apps-properly/13378)

## Build /e/ for a device

 - [[HOWTO] Build the /e/ ROM ](https://doc.e.foundation/how-tos/build-e)
 - [[HOWTO] How much space do I need to build for /e/?](https://community.e.foundation/t/howto-how-much-space-do-i-need-to-build-for-e/3014)
 - [[HOWTO] Some useful Docker commands](https://community.e.foundation/t/howto-some-useful-docker-commands/3075)
 - [[HOWTO] Find files in your source tree](https://community.e.foundation/t/howto-find-files-in-your-source-tree/4794)
 - [[HOWTO] An unofficial / e / ROM built with Docker – in pictures](https://community.e.foundation/t/howto-an-unofficial-e-rom-built-with-docker-in-pictures/2445)
 - [[HOWTO] I am trying to build /e/ but the download seems to be stuck](https://community.e.foundation/t/howto-i-am-trying-to-build-e-but-the-download-seems-to-be-stuck/3013)
 - [[HOWTO] Build /e/ without docker](https://community.e.foundation/t/howto-build-e-without-docker/3149)
 - [[HOWTO] Build /e/ without docker for non LineageOS supported devices](https://community.e.foundation/t/howto-build-e-without-docker-for-non-lineageos-supported-devices/3991)
 - [[HOWTO] Building unofficial /e/ rom on a cloud VM - various options](https://community.e.foundation/t/howto-building-unofficial-e-rom-on-a-cloud-vm-various-options/6877)
 - [[HOWTO] Build /e/ the full classic way no docker, no scripts, just ‘make’](https://community.e.foundation/t/howto-build-e-the-full-classic-way-no-docker-no-scripts-just-make/19364)
 - [[HOW TO] First steps if your build won’t boot](https://community.e.foundation/t/how-to-first-steps-if-your-build-wont-boot/5480)

## Read the [HOWTOs] In French
 - [[HOWTO] Installer toutes les applis du Google Play Store grâce à Aurora Store](https://community.e.foundation/t/howto-installer-toutes-les-applis-du-google-play-store-grace-a-aurora-store/13623)
 - [[HOWTO] Obtenir un e-mail et un compte /e/](https://community.e.foundation/t/howto-obtenir-un-e-mail-et-un-compte-e/11892)
 - [Comment installer /e/ sur smartphone Android](https://community.e.foundation/t/comment-installer-e-sur-smartphone-android/11024)
 - [[HOWTO] Installer /e/!](https://community.e.foundation/t/howto-installer-e/3936)
 - [[TUTO] Capture d’écran sur Galaxy S7, S9, etc](https://community.e.foundation/t/tuto-capture-decran-sur-galaxy-s7-s9-etc/5742)
 - [[TUTO] ERROR 7 : Vendor image on the device is not compatible](https://community.e.foundation/t/tuto-error-7-vendor-image-on-the-device-is-not-compatible/4685)
 - [[TUTO] Tutoriel en français : installation, configuration, usage](https://community.e.foundation/t/tuto-tutoriel-en-francais-installation-configuration-usage/11477)
 - [[HOWTO] Sauvegarder mon téléphone](https://community.e.foundation/t/howto-sauvegarder-mon-telephone/3935)
 - [[HOWTO] Deverrouiller mon mobile + Installation Recovery (TWRP)](https://community.e.foundation/t/howto-deverrouiller-mon-mobile-installation-recovery-twrp/3395)
 - [[HOWTO] Mon mobile est-il compatible?](https://community.e.foundation/t/howto-mon-mobile-est-il-compatible/3393)
 - [[HOWTO] Installer /e/ sur un smartphone Samsung avec Windows facilement](https://community.e.foundation/t/howto-installer-e-sur-un-smartphone-samsung-avec-windows-facilement/3184)
 - [[HOWTO] Mettre à jour /e/](https://community.e.foundation/t/howto-mettre-a-jour-e/3220)
 - [[HOWTO] Importer ses agendas](https://community.e.foundation/t/howto-importer-ses-agendas/2967)
 - [[HOWTO] Importer ses contacts](https://community.e.foundation/t/howto-importer-ses-contacts/2943)
 - [[TUTORIEL] GUIDE FRANCAIS DE LA COMMUNAUTE ET GITLAB /e/](https://community.e.foundation/t/tutoriel-guide-francais-de-la-communaute-et-gitlab-e/2612)

## Read the [HOWTOs] In German

 - [[HOWTO] Wie erstelle ich eine /e/-Email-ID](https://community.e.foundation/t/howto-wie-erstelle-ich-eine-e-email-id/12009)
 - [[HOWTO] Forum Sprache auf Deutsch umstellen](https://community.e.foundation/t/howto-forum-sprache-auf-deutsch-umstellen/743)

## How to Support /e/
- [Be part of /e/](https://e.foundation/get-involved/)
- [Crowdfunding to fuel the /e/ adventure!](https://e.foundation/donate/)
