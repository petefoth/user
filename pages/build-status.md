---
layout: page
title: Different Build Types
permalink: build-status
search: exclude
toc: true
---

### Dev:
 - Builds are made by /e/ using the /e/ build infrastructure and signed by /e/ dev keys
 - Include latest /e/ developments
 - Have been tested and considered stable enough for a normal use
 - Usually official builds have a maintainer
 - Get regular OTA updates
 - Available for download on /e/ websites
 - Release Frequency: Weekly 
 - Build Naming format: e-0.7-n-2020021341116-dev-mata.zip
 > Naming format explanation:

 > e-codeversionnumber-osversion-YYYYMMDDbuildid-dev-devicename.zip

### Stable
 - These builds are currently only available on the /e/ smartphones
 - Are built using a freeze of the Dev branch, with additional and longer testing
 - Builds are made by /e/ using the /e/ build infrastructure and signed by /e/ stable keys
 - Official builds have a maintainer
 - Get regular OTA updates
 - Release Frequency: Monthly or bimonthly
 - Build Naming format: e-0.7-n-2020021341116-stable-mata.zip
 > Naming format explanation:

 > e-codeversionnumber-osversion-YYYYMMDDbuildid-stable-devicename.zip

### Test
  - Builds are made by /e/ using the /e/ build infrastructure and signed by Android test keys
  - Builds are made to test some new features or an upgrade
  - May or may not get regular OTA updates
  - These builds are only accesible to testing team members  
  - Are not available for download on /e/ websites
  - Release Frequency: Depending on testing requirement
  - Build Naming format: /e/OS-0.7-n-2020021341116-test-mata.zip
  > Naming format explanation:

  > e-codeversionnumber-osversion-YYYYMMDDbuildid-test-devicename.zip

### Unofficial
 - Builds are made by /e/ or external users
 - Made using /e/ sources code with no modifications in any form
 - At times may not have a maintainer as in made by a user who does not own the device
 - Do not receive OTA updates
 - Available for download on /e/ websites and are tagged as "UNOFFICIAL"
 - Release Frequency: Monthly or biMonthly
 - Build Naming format: e-0.7-n-2020021341116-UNOFFICIAL-mata.zip
 > Naming format explanation:

 > e-codeversionnumber-osversion-YYYYMMDDbuildid-UNOFFICIAL-devicename.zip

### Custom
   - Builds are made by /e/ or external users
   - The build may have possible changes to /e/ sources and include code developed for specific device support
   - Builds are not supported by /e/
   - Will not be publicly available for download on /e/ websites
   - Release Frequency: Dependent on the client or ROM Builder
   - Build Naming format: Format to be set by client or ROM Builder
