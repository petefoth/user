---
title: Sync contacts in Webmail on Rainloop
layout: page
permalink: /how-tos/sync-contact-webmail
toc: true
---

## Purpose of the document
/e/cloud users have the option to send emails using the [Rainloop](https://www.rainloop.net/) webmail.

This is simple, faster and a lot easier than configuring a client on every desktop you access.

This guide explains how to configure the Webmail Ui available on eCloud to sync with the eCloud contacts.


## Steps to integrate
The process to integrate your contacts with the Rainloop webmail UI is as under.

### Logging in

- Log into [ecloud](https://ecloud.global)
- If you do not already have an /e/ account you can get one [here](/create-an-ecloud-account) for free

### Getting contact info

- Go to the contact tab

    ![](/images/rainloop_contacts_tab.png)
- In the Contacts screen go to `Settings` > `3 dots` > `Copy link`

    ![](/images/rainloop_copy_link.png)

### Browse to Mail

- Return to the main screen and go to Mail tab

    ![](/images/rainloop_mail_tab.png)
- This would open the Rainloop webmail UI

### Rainloop settings screen

- You can access the rainloop settings by clicking or tapping the gear icon on the lower left hand side of the screen

    ![](/images/rainloop_settings.png)

    In the rainloop settings screen
- Activate sync checkbox ‘Enable remote syncronization’
- Paste the url you copied above in the Addressbook URL textbox
- Authenticate with /e/ credentials - user and password for /e/ ID to be used

### Check the sync in mail UI

- It may take a few minutes for the contacts to show up. So relax and wait for the process to complete.  

    ![](/images/rainloop_webui.png)

{% include alerts/tip.html content="Contacts from NextCloud address books should also merge with rainloop ones"%}

{% include alerts/tip.html content="While you are in the Rainloop setting screen, check out the options to set filters. Using that you can direct your mail to specific folders. Here you can also set up some colorful themes."%}
