---
layout: page
title: microG EN update
permalink: microg-en-update
---

The new **microG EN version** add some improvements in the Exposure Notification API. Unfortunately, the **update** we shared on Apps **doesn't apply automatically**. To apply it manually, you have to:

1. Open `Apps`
1. In `Search` tab, search for `microg`
1. Tap on `Update`


If you **apply** the v0.14 update **before** having update microG, microG EN will be uninstalled. In case it happens, no worry:

From your device, download and install manually microG EN version:
- [/dev devices](https://gitlab.e.foundation/api/v4/projects/149/jobs/artifacts/e-v0.2.17.204714/raw/play-services-core/build/outputs/apk/withMapboxWithNearby/releaseDev/play-services-core-withMapbox-withNearby-releaseDev.apk?job=build-release)
- [/stable devices](https://gitlab.e.foundation/api/v4/projects/149/jobs/artifacts/e-v0.2.17.204714/raw/play-services-core/build/outputs/apk/withMapboxWithNearby/releaseStable/play-services-core-withMapbox-withNearby-releaseStable.apk?job=build-release)

*Be sure we will do our best to avoid this inconvenience in the future!*
