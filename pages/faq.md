---
layout: page
title: /e/ FAQ
permalink: faq
toc:  true
---

### Is /e/ open source?

Yes - all source code is available and you can compile it, fork it... Some prebuilt applications are used in the system; they are built separately from source code available here, or synced from open source repositories such as F-Droid. We ship one proprietary application though ([read the statement](/maps)).

### Cool, /e/ is open source - how can I contribute?

- join one of the /e/ [projects looking for contributors](/projects-looking-for-contributors)
- help solving [issues listed here](https://gitlab.e.foundation/groups/e/-/issues)
- [start building](build-e) /e/ for a new device and become a maintainer

Maybe you have other suggestions, feel free to contact us! Send an email to <join@e.email>, with a clear subject line (like: ROM Developer Mediatek).

### Is /e/ LineageOS + microG?

/e/ is forked from LineageOS. We've modified several parts of the system (and we're just beginning): installation procedure, settings organization, default settings. We've disabled and/or removed any software or services that were sending personal data to Google (for instance, the default search engine is no longer Google). We've integrated microG by default, have replaced some of the default applications, and modified others. We have added a synchronization background software service that syncs multimedia contents (pictures, videos, audio, files...) and settings to a cloud drive, when activated.

Also, we've replaced the LineageOS launcher with our own new launcher, written from scratch, that has a totally different look and feel from default LineageOS.

We've implemented several /e/ online services, with a single /e/ user identity (user@e.email). This infrastructure will be offered as docker images for self hosting: drive, email, calendar... to those who prefer self-hosting.

We have added an account manager within the system with support for the single identity. It allows users to log only once, with a simple "user@e.email" identity, for getting access to /e/'s various online services (drive, email, calendar, notes, tasks).

We now have a /e/ product description [here](what-s-e)

### Why should I use /e/? I can customize the ROM myself...

Absolutely! /e/ was not made for geeks or power users, it's designed to offer a credible and attractive alternative to the average user, with more freedom and a better respect of user's data privacy compared to mobile operating systems offered by the worldwide duopoly in place.

We really appreciate your involvement though. Your beta-testing and valuable feedback will help to make this alternative available to many more people who can't do this sort of thing for themselves.    

### I'm a geek and I love the default AOSP/LineageOS look

In that case, you probably won't be very happy with /e/, don't use it!

### Aren't you stealing the work of LineageOS developers?

No - we're using the rules of open source software. Just like AOSP-Android is forking the Linux kernel work, just like LineageOS is forking AOSP work, /e/ is forking LineageOS work. /e/s focus is on the final end-user experience, and less on the hardware. /e/OS and LineageOS's purposes and target users are different. However, we strongly encourage core developers to contribute upstream to LineageOS. When thinking about LineageOS vs /e/, think about Debian vs Ubuntu.


### Does /e/ have a simple way for updating the system regularly?

Yes - a simple 1-click (well, that may be 2 or 3!) "over the air" update feature is available in settings.

### Is my XYZ123 device supported?

Look at the [device list](/devices/). We are adding new devices regularly.

### From which version of Android is /e/ forked?

 - Android 7 (Nougat)/LOS 14 (/e/-0.x-N branch).
 - Android 8 (Oreo)/LOS 15 (/e/-0.x-O branch).
 - Android 9 (Pie)/LOS 16 (/e/-0.x-P branch).
 - Android 10 (Q)/LOS 17 (/e/-0.x-Q branch).

### Is /e/ stable?

No - we're in beta stage at the moment. The system is pretty usable though, but use it at your own risk!

### What is the current state of De- googlisation on /e/ ?

For a detailed response please go through the document given [here](https://e.foundation/wp-content/uploads/2020/09/e-state-of-degooglisation.pdf)

### Is there a way to easily check if the services offered by /e/ are online

This url lists [the status of servers](https://status.ecloud.global/) on which /e/ services are hosted
### Will you offer /e/-preloaded phones?

Yes - refurbished smartphones flashed with /e/OS, [are now available](https://e.foundation/e-pre-installed-smartphones/).

### If I find a bug, should I send you an email?

No - please report bugs and send suggestions using this platform as [explained here](/how-tos/report-an-issue). Please avoid using email or IM for reporting issues, it's not an efficient process.

### If I have suggestion, how should I contact you?

You can also use this platform to report suggestions. If you have suggestions related to improve privacy, you can also send an email to privacychallenge@e.email

### Why this weird, inconvenient /e/ name?

We had to change our name for legal reasons [as explained here](https://www.indidea.org/gael/blog/leaving-apple-and-google-e-is-the-symbol-for-my-data-is-my-data/).

/e/ is a textual representation of our "e" symbol which means "my data is MY data".

It's the current project codename, we will probably introduce a new and more convenient name for our mobile ROM in a few months.

### I have some questions about the Apps Store ?
Please check the [Apps store FAQ](apps) for the answers to all your queries.


### My device is no longer supported by LineageOS. Will /e/ also stop support?

/e/ is not dependent on LineageOS in deciding which devices to support or stop supporting. We already have several devices which are not on the LineageOS supported list. We plan to port more devices to work with /e/ code.

### If my device is not supported by LineageOS will I still get the security patches on time?

Yes. We will download and apply the security patches for devices if they are available for download from the source.

### Is my data on the /e/ servers encrypted?

  - All user data is encrypted on the main block storage and in backups too.
  - True E2E (end to end) encryption is in our plans as a long-term feature.

### How can I delete my /e/ ID?

* Log in to your [ecloud account](https://ecloud.global/index.php/login) using the ID you want to delete
* Take a backup of your data and emails (if required) before proceeding
* Click on your profile image
* This will open up a menu
* Click on Settings
* This will display the Setting options as shown in the screenshot below

![](images/Account_delete.png)

* Click on 'Delete Account'
* This will open up a screen with the text "Check this to confirm the deletion request"
* Select the check box
* Click 'Delete my Account' button
* The operation could take up to a minute. Please wait
  >  This action will remove your /e/ ID and ecloud account , data stored on the ecloud if any and all emails.

* You will be logged out of [ecloud.global](https://ecloud.global/index.php/login)
* The /e/ account deletion is complete
> In case of any issues please send in the details to <support@e.email>

### What can I do if I lost my phone?
> For users who had an /e/ ID registered on the phone you lost

The steps to be taken are as under:
- Request an account deactivation by sending a mail to <support@e.email>
- For the request use your /e/ mail ID or the recovery email you used when you created the /e/ ID
we have both email ID's in our database and will be able to help you deactivate your ID
- To explain further here you request for an account deactivation. Do not Delete your account as that will  disable your access to your account data on the [eCloud](https://ecloud.global) as well.
### Some additional information if you lost your phone
As you may be aware eCloud is forked from NextCloud.

Next cloud has two features which help in such situations
 - Revoke Session

  The Revoke  Session feature is done from the server.
  You can view this in your ecloud under `settings/user/security >devices & sessions`

 - Wipe Device

  This implementation has not been added as yet into the OS.

### Can the eCloud admin access my data

On any Nextcloud instance, an administrator can see your files and all the information in the database. Not only NextCloud admins, Server admins (ie root) have this access. The only way to make files inaccessible is by using end-to-end encryption.

Nextcloud has no plans for database encryption as per our understanding. We are planning to provide an additional service down the line, maybe based on [EteSync](https://www.etesync.com/). The team needs to investigate its feasibility and UX (normally it would mean losing the ability to access that info from the web interface).

Our eCloud admins need to make backups, perform upgrades, reset passwords, etc.
We are planning to enable end to end encryption (E2EE) plugin in the second half of the year 2021 . 

This will allow you to store files secured with a key only you would possess, while still letting our eCloud admins do the work of maintaining the platform.

You can also host your own [ecloud](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) or a [plain Nextcloud](https://nextcloud.com/athome/) (no e-mail server included in that case).

### I want to know more about the security on eCloud

Since this is a common query from /e/ users, we would like to clarify a few points:

  -  ecloud.global is a modified version of [ecloud-selfhosting](https://gitlab.e.foundation/e/infra/ecloud-selfhosting), which is based upon several open source projects.
  -  Nextcloud server-side encryption is not used, instead [full-disk encryption](https://nextcloud.com/blog/encryption-in-nextcloud/) is used. We should enable SSE soon as it's a requirement for E2EE.
  -  We have a long-standing relationship with a security expert in charge of hardening and monitoring our systems, including ecloud.global.

A few of the improvements applied to ecloud.global in regards to the base ecloud-selfhosting instance:

  -  Performance tuning adapted to the scale of the service (number of users, storage size, etc).
  -  High availability for all core services: nextcloud, mariadb and redis, behind a HAProxy load balancer.
  -  We try to always keep the infrastructure and applications in compliance with the best available security hardening guidelines available such as [DevSec Hardening Framework](https://github.com/dev-sec) and CIS Benchmark for Ubuntu.
  -  We apply process confinement techniques and mitigation provided by [AppArmor](https://apparmor.net/) and systemd.
  -  We use [Wazuh](https://wazuh.com/) monitoring for threat detection.

If you think (or can prove) that there is a security vulnerability in ecloud.global or the ecloud-selfhosting project, please contact us directly: <security@e.email> 

Kindly use the following key to encrypt any sensitive disclosure:
[https://keys.openpgp.org/vks/v1/by-fingerprint/F242DB4B0F002ED0AB73A5D06E25E121E5939DAF](https://keys.openpgp.org/vks/v1/by-fingerprint/F242DB4B0F002ED0AB73A5D06E25E121E5939DAF)

### Updates coming to eCloud
  Besides the **Wipe Device** feature the **Find my phone** feature is planned for implementation in Q3 of 2021




### Other questions

Some more technical questions & answers are available [in a specific FAQ](https://community.e.foundation/t/e-os-smartphones-faq/6108)
