---
layout: page
title: Apps Installer
permalink: apps
search: exclude
toc: true
---

## Where do the Apps in /e/ installer application come from?

Apps available in the /e/ Installer are open source apps that you can find on Fdroid, and free Android apps, same as you can find on  Google Play Store. The /e/ app installer relies on a third-party application repository called [cleanapk.org](https://info.cleanapk.org)

Soon, mobile web apps and progressive web apps will also be available through the /e/ application install.

## Where is the source code base of the /e/ installer application?

You can check the code here [https://gitlab.e.foundation/e/apps/apps](https://gitlab.e.foundation/e/apps/apps)

## Is there an easy way to find if an application I want is available in the /e/ App Store ?

You can search for the name of the application in the [Application Checker](https://e.foundation/e-os-available-applications/)

## How can I request an application to be added to the library?

Users can request  a specific app  to be listed by making a query directly in the /e/ application installer (**Settings** > **Request App**)

## What information is provided for each app within the /e/ application installer ?

At the moment, each app features a Privacy rating. In the future, user rating and energy score will also be available. We will also provide classic user ratings to help user make a better choice

Privacy ratings provide a list of trackers and requested permissions that come in as part of installing these apps (Note: those are computed by Exodus Privacy software). Energy rating will provide details of how much energy is consumed by the app to work. Please note  some of these features are currently under development.

## How can I make sure  apps in the installer are not tampered with but ‘original'

Apps are checked either using a PGP signature check, or a checksum.

In case you suspect an app  to be tampered, we would request you to please report the same here <support@e.email> and we will take appropriate action keeping you informed.

## What should you do  if you don't find an app you would like to use in the /e/ app  installer ?

If you can't find an Android app you would like to use, you can first request it in the app installer

The process to request an app on /e/'s App Store is as under
- Tap the Apps Icon
- On the Apps task bar

    you will see `Settings` > `Request App`

    and in the text box provide the `package name` of the app you want to be added.
- To get the package name you can
    - Go to the Google Play store.
    - Select the app download page of your app.

      > For e.g. let us assume you want to add the [Radar Covid app](https://play.google.com/store/apps/details?id=es.gob.radarcovid) to the /e/ Apps Store

    - When you open the app download page on the Google Play Store check the page url - the package name will be the last couple of words which in this case would be  `es.gob.radarcovid`
    - Copy this and paste it in the text box
    - Press the `Submit` button

    > You will see a success message displayed as the request is accepted.

    > In case the app has already been requested you will see a message mentioning that it has already been requested.

- An option is also to look for a web alternative . For instance, several services  are available with  progressive web apps that can be used on the mobile.: Uber, Twitter, Starbucks, Some banks are alos offering web services as  an alternative to native apps. For  easier access, a shortcut to the web service can easily be added to the /e/ BlissLauncher, using the web browser.

## How long would it take for an app I requested to show up in /e/ Apps Store

  It takes about 1-2 weeks for the app to show up in the /e/ Apps Store after the request.

  If an app does not show up in the /e/ Apps Store even after a couple of weeks of submitting the request, please raise an issue in [/e/ Gitlab](https://gitlab.e.foundation/e/backlog/-/issues/new) with all the details.

## PWA

Another option is also to look for a web alternative . For instance, several services  are available with  progressive web apps that can be used on the mobile.: Uber, Twitter, Starbucks, Some banks are also offering web services as  an alternative to native apps. For  easier access, a shortcut to the web service can easily be added to the /e/ BlissLauncher, using the web browser.

## Alternate Application Stores

Some users have also reported some success using applications downloaded using the Yalp Store or Aurora Store. Though these stores will require the user to authorize third-party application installation in /e/OS settings.
