---
layout: page
title: How to sync new ecloud calendars
permalink: /how-tos/how-to-sync-new-ecloud-calendars
search: exclude
---

When a new calendar is created on ecloud, it is not automatically available on the phone. Here the process to sync it:

1. From the device, open `Settings` > `Accounts`

    ![](/images/screenshot_20200624_15:22:06.png)
    ![](/images/screenshot_20200624_15:22:09.png)
1. Tap on your device account (not the Contacts entry!)

    ![](/images/screenshot_20200624_15:22:12.png)
1. Tap `Account Settings`

    ![](/images/screenshot_20200624_15:22:16.png)
1. In the new Window, on your /e/ email address

    ![](/images/screenshot_20200624_15:22:20.png)
1. Go into `Calendar` tab

    ![](/images/screenshot_20200624_15:22:23-bis.png)
1. Refresh the calendar list by clicking on the `3 dots button` (to right) > `Refresh calendar list`

    ![](/images/screenshot_20200624_15:22:26.png)
1. When few seconds. Your new calendar is now available!

    ![](/images/screenshot_20200624_15:22:23.png)
1. The new calendar is also available in the Tasks app

    ![](/images/screenshot_20200624_15:22:35.png)
