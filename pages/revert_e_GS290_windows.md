---
layout: page
title: Reverting to stock ROM on the GS290 for Windows users
permalink: pages/revert_e_GS290_windows
---

{% include alerts/danger.html content="These instructions are only for Windows PC users"%}

{% include alerts/danger.html content="The install will wipe all data on your phone."%}

1. Download and install the [SP Flash Tool](https://spflashtool.com/download/)
1. Next  open sp flash tool
1. Go to download tab **NEVER** use format
1. In scatter file select `MT6763_Android_scatter.txt` inside the extracted firmware
1. Press download
1. Turn off the phone
1. Plug in the phone ..it will ask you to plug in at this point
1. Now it will be downgraded to android 10
1. This process will wipe all your existing data
1. Long press on power button to restart the phone

Click here to [return](/devices/GS290/install#before-flashing) to continue the installation of /e/OS