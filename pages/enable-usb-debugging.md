---
layout: page
title: Enable USB Debugging
permalink: /pages/enable-usb-debugging
search: exclude
toc: true
---

## To enable USB Debugging

- You will need to become a `Developer` first on your device !!

## To become a developer
- Unlock your device and go to 

`Settings` > `About Phone` > `Build Number or Version` > `Tap seven times`


You will get a message that you are a Developer.

## To Allow USB Debugging
After completing the above steps and becoming a developer on your device go to 


`Settings` > `Additional Settings` > `Developer Options` > `Toggle Developer Options to enable` > `Toggle USB Debugging to enable`

On some devices the path may be as under 

`Settings` >> `System` >> `Advanced` >> `Developer options` >> `USB Debugging ( enable it here)`

Return to main screen
## To unlock OEM
### Why do you need to enable `Unlock OEM`
To flash a custom ROM like LineageOS or /e/OS you need to unlock the bootloader of your device. This requires that you as the owner of the device should 
- Grant permission to unlock the bootloader
- Unlock the bootloader 

### Steps to enable `OEM Unlock` through the `Developer Options`

`Settings` > `Additional Settings` > `Developer Options` > Search for `OEM Unlock` and enable

On some devices the path may be as under 

`Settings` >> `System` >> `Developer options` >> Search for `OEM Unlock` and enable

### Steps to enable `OEM Unlock` through the command line

- Enable USB debugging on your phone and connect it to the pc by a USB cord. The instructions for this are given above
- Open a console window 
- Enter `adb devices` 
- Proceed only if your device is detected. 
- Enter `adb reboot bootloader`
- Enter `fastboot devices`, proceed only if the device is detected
- Enter `fastboot oem unlock`
- You will be asked to confirm the request on the phone screen, select `yes`
- When the process completes, type `fastboot reboot` and the phone will reboot normally.


Return to main screen