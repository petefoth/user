---
layout: page
title: How to Build the /e/ ROM?
permalink: how-tos/build-e
---

## Definitions: Custom vs Unofficial vs Official

A **custom build** is a build that is using most of the /e/ source code to generate a new ROM. It can be modified in any manner by the developer, for any purpose.

An **unofficial /e/ build** is a build from the /e/ source code that is intended to generate the /e/ ROM for a new, not yet supported, device. It’s not been approved officially and is not included yet to the list of official /e/ ROMs. It can be published publicly tagged as "unofficial".

An **official /e/ build** is an unofficial build from /e/ source code that has reached a sufficient level of quality and has been validated. It has been published publicly as an official build on /e/ websites.

## Requirements for differents builds:

* **Custom builds**: if the source code has been modified and if the ROM is redistributed, we have several requirements:
    * any reference to /e/, including “/e/”, and our logo must be removed from the build
    * the redistribution must not let people think that it is /e/. **The name must be changed** and possibly say that it’s been forked from /e/
    * calls to our OTA server infrastructure should be removed
    * Custom ROMs can be built on /e/ servers only for exceptional reasons (special development…).

* **Unofficial /e/ builds**: source code can be modified only to reach full support on the target device. /e/ features, list of installed apps etc. shouldn’t be modified. Unofficial builds can be published on the /e/ website at a specific place with warnings and a list of what works/what doesn’t work. It doesn’t have nightly builds nor OTA updates.

* **Community builds**: the level of quality is considered high, security updates are applied if possible, but there is not yet or no more official maintainer. Community builds have nightly builds and can be updated OTA. Source code have to be hosted on our GitLab instance, or on trusted sources (LineageOS GitHub group, AOSP).

* **Official /e/ builds**: the level of quality must have reached our conditions. It must have an official maintainer. Official builds have nightly builds and can be updated OTA. Source code have to be hosted on our GitLab instance, or on trusted sources (LineageOS GitHub group, AOSP).

Any question about this? [contact us](https://e.foundation/contact/)

# How to build the ROM?

> WARNING : This process will only work on **case-sensitive** filesystems!
> * Windows: will not work
> * macOS: doesn't work either for HPS+ filesystem - adfs not tried
> * Linux: work on Ubuntu and CentOS

> System requirement: Your computer needs to be 64-bit and running a 64-bit operating system with at least 250GB of spare hard drive space and 8GB of RAM (or 16GB in a virtual machine).

## 1. Install docker

If you have not already done so, [install docker](https://docs.docker.com/install/)

## 2. Get our docker image

> Please run this step before each build, to be sure to get the last docker image.

```shell
$ sudo docker pull registry.gitlab.e.foundation:5000/e/os/docker-lineage-cicd:community
```

## 3. Create directories

```shell
$ sudo mkdir -p \
/srv/e/src \
/srv/e/zips \
/srv/e/logs \
/srv/e/ccache \
```

## 4. Start build

Run the following command. Don't forget to replace `<my-device>` with your device code !

```shell
$ sudo docker run \
-v "/srv/e/src:/srv/src" \
-v "/srv/e/zips:/srv/zips" \
-v "/srv/e/logs:/srv/logs" \
-v "/srv/e/ccache:/srv/ccache" \
-e "BRANCH_NAME=<tag>" \
-e "DEVICE_LIST=<my-device>" \
-e "OTA_URL=<ota-server-url>" \
-e "REPO=https://gitlab.e.foundation/e/os/releases.git" \
registry.gitlab.e.foundation:5000/e/os/docker-lineage-cicd:community
```

List of tags to use for `BRANCH_NAME` is available at [https://gitlab.e.foundation/e/os/releases/-/tags](https://gitlab.e.foundation/e/os/releases/-/tags).

The device code can be found on [/e/ devices list](../devices), [LineageOS wiki](https://wiki.lineageos.org/devices/) or with the following command: `$ adb shell getprop ro.product.device`

> We now use git tags in addition to a specific manifest for each release. It let us to know exactly what's inside each build.

> If you want to build a test version, you can use:
> - `BRANCH_NAME` set to v1-nougat, v1-oreo or v1-pie to get the last test build
> - `BRANCH_NAME` set to v1-nougat, v1-oreo or v1-pie & `REPO=https://gitlab.e.foundation/e/os/android.git`

---

Example for Samsung Galaxy S9
```shell
$ sudo docker run \
-v "/srv/e/src:/srv/src" \
-v "/srv/e/zips:/srv/zips" \
-v "/srv/e/logs:/srv/logs" \
-v "/srv/e/ccache:/srv/ccache" \
-e "BRANCH_NAME=v0.9-pie" \
-e "DEVICE_LIST=starlte" \
-e "OTA_URL=<ota-server-url>" \
-e "REPO=https://gitlab.e.foundation/e/os/releases.git" \
registry.gitlab.e.foundation:5000/e/os/docker-lineage-cicd:community
```

## 5. Build options

You can now customize applications installed by default in /e/OS.

* if you want to add extra applications to the default applications: add your APK to the android_prebuilts_prebuiltapks/ directory, and set the `CUSTOM_APPS` environment variable accordingly in the Docker image, before building. 
* if you want to keep a minimal /e/OS build, set the `MINIMAL_APPS` environement variable to true (default is false). For now it's removing LibreOffice viewer, PDFViewer, Maps and Weather.
 
## 6. Get your image!

When your build is finished, please find your images inside `/srv/e/zips/<my-device>` folder. To install, please refer to our [documentation](../../devices).

If you need help, please join us on our [community support Telegram channel](https://t.me/joinchat/Fzzi3kUbP-AcoQz3zYHl5A).

To find more information about our docker image and its environment variables [here](https://gitlab.e.foundation/e/os/docker-lineage-cicd).

To report an issue about a build, please refer to [issues documentation](report-an-issue)
