---
layout: page
permalink: /pages/unauthorized-device
search: exclude
title: Fix unauthorized device error in adb
---

When running <code>adb devices</code> in bash a device is shown as <code>unauthorized</code> 

1. Disconnect USB between PC and device.
1. Stop adb server by entering <code>adb kill-server</code>  in command window.
1. On device use <kbd>Revoke USB debugging authorizations</kbd>  in <kbd>Developer 
Options</kbd>
1. On PC delete <code>adbkey</code>  file in user directory, eg. <code>.android</code> 
1. Reconnect the device to the PC.
1. Check the box on the popup on the device.
1. Open a bash and enter <code>adb devices</code> 

Device should be shown as shown in this screenshot
<br>
![](/images/adb_Device_Detected.png)