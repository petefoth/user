---
layout: page
title: Build Heimdall
permalink: how-tos/build-heimdall
---

The Heimdall project does not provide package for the last version. So you need to build it manually from [our fork](https://gitlab.e.foundation/e/tools/heimdall). Bellow the documentation for each platform, extracted from README of Heimdall project.

## Download Heimdall sources

Download from [Heimdall source code repository](https://gitlab.e.foundation/e/tools/heimdall).

## If you are building on Windows (Source: [Heimdall/Win32/README.txt](https://gitlab.com/BenjaminDobell/Heimdall/blob/master/Win32/README.txt#L518))

Heimdall and Heimdall Frontend both utilise CMake for managing the build process. CMake can generate files for various build systems including GNU Make and Visual Studio. However, official packages are compiled with GNU Make and MinGW-W64 GCC/G++.

**NOTE**: Official builds use MinGW-W64 simply because on-going cross-platform development is simpler when using just the one IDE (Jetbrain's CLion) and similar toolchains.

1. Setup a MinGW-W64 build environment by utilising MSYS2: http://msys2.github.io/
2. After installing MSYS2 a command prompt will launch, enter:
```
Pacman -Syu
Pacman -S mingw-w64-x86_64 mingw-w64-x86_64-clang mingw-w64-x86_64-cmake mingw-w64-x86_64-libusb mingw-w64-x86_64-qt5-static make
 ```
3. Add the MinGW-W64 binaries to your PATH environment variable:
```
export PATH="/mingw64/bin:$PATH"
```
4. Build Heimdall & Heimdall Frontend
```
mkdir build
cd build
cmake -G "MSYS Makefiles" -DCMAKE_BUILD_TYPE=Release -DQt5Widgets_DIR=/c/msys64/mingw64/qt5-static/lib/cmake/Qt5Widgets ..
make
```

## If you are building on Linux (Source: [Heimdall/Linux/README.txt](https://gitlab.com/BenjaminDobell/Heimdall/blob/master/Linux/README#L475))

1. First make sure you have installed `build-essential`, `cmake`, `zlib1g-dev`, `qt5-default`, `libusb-1.0-0-dev` and `OpenGL` (e.g `libgl1-mesa-glx` and `libgl1-mesa-dev`). **NOTE**: Package names may not be absolutely identical to those above.
2. Open a terminal and navigate to the directory you downloaded, or extracted, Heimdall to.
3. Enter the following commands to compile Heimdall Suite:
```
mkdir build cd build cmake -DCMAKE_BUILD_TYPE=Release .. make
```

## If you are building on Mac OSX (Source : [Heimdall/OSX/README.txt](https://gitlab.com/BenjaminDobell/Heimdall/blob/master/OSX/README.txt#L480))

1. Apply following patch:
    * [https://gitlab.com/bfabiszewski/Heimdall/commit/49416216c6d131769521c7f2c573cc78455941b2](https://gitlab.com/bfabiszewski/Heimdall/commit/49416216c6d131769521c7f2c573cc78455941b2)
1. Open Terminal and install dependencies using Homebrew (http://brew.sh)
```
brew install libusb qt5 cmake
```
1. Build Heimdall & Heimdall Frontend
```
mkdir build
cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DQt5Widgets_DIR=/usr/local/opt/qt5/lib/cmake/Qt5Widgets ..
make
```

*This documentation "Install Heimdall v1.4.2" is a derivative of [Win32/README.txt](https://gitlab.com/BenjaminDobell/Heimdall/blob/master/Win32/README.txt), [Linux/README.txt](https://gitlab.com/BenjaminDobell/Heimdall/blob/master/Linux/README) and [/OSX/README.txt](https://gitlab.com/BenjaminDobell/Heimdall/blob/master/OSX/README.txt) by [Heimdall Project](ttps://gitlab.com/BenjaminDobell/Heimdall), used under [MIT License](https://gitlab.com/BenjaminDobell/Heimdall/blob/master/LICENSE).*
