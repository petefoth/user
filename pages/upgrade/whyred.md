---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/whyred/upgrade
device: whyred
---
{% include templates/device_upgrade.md %}