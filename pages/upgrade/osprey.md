---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/osprey/upgrade
device: osprey
---
{% include templates/device_upgrade.md %}