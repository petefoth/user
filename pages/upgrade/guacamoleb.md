---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/guacamoleb/upgrade
device: guacamoleb
---
{% include templates/device_upgrade.md %}