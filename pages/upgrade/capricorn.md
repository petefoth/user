---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/capricorn/upgrade
device: capricorn
---
{% include templates/device_upgrade.md %}
