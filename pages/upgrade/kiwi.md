---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/kiwi/upgrade
device: kiwi
---
{% include templates/device_upgrade.md %}
