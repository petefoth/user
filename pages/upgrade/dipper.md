---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/dipper/upgrade
device: dipper
---
{% include templates/device_upgrade.md %}