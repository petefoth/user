---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/marlin/upgrade
device: marlin
---
{% include templates/device_upgrade.md %}