---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/beryllium/upgrade
device: beryllium
---
{% include templates/device_upgrade.md %}
