---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/pioneer/upgrade
device: pioneer
---
{% include templates/device_upgrade.md %}