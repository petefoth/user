---
sidebar: home_sidebar
folder: upgrade
layout: page
title: /e/ OS Version Upgrade
permalink: /devices/s3ve3gxx/upgrade
device: s3ve3gxx
---
{% include templates/device_upgrade.md %}