---
layout: page
title: Create a log @/e/
permalink: /how-tos/create-a-log
search: exclude
---

## Why do you need a log?

Whenever you run into an issue and try to report it you must have seen the request from the support or development teams to provide a log or a logcat.

A log helps the development team debug the issue and find out the resolution.  

To generate a logcat you would need to enable `adb` on your phone as well as your PC.

## What is `adb`?
`adb` stands for Android Debug Bridge. `adb` is a versatile command-line tool that lets you communicate with a device. The `adb` command facilitates a variety of device actions, such as installing and debugging of apps.

## To enable adb

1. On your phone

   - Enable developers options in settings (**Settings** > **About phone** > Tap **Build number** 7 times)
   - Enable Android debug (**Settings** > **System** > **Developer options** > **Android debugging**)

1. On your computer,
    - install `adb` on your PC
    - There could be issues here with the device driver or the OS type. You can find details on how to install `adb` [here](https://www.xda-developers.com/install-adb-windows-macos-linux/)

    - connect your device to your computer
    - Detect your device by typing the below command at the command prompt

      ```shell
      $ adb devices
      ```

      You would have to accept an authorization on your phone. A pop up like this will appear

      ![Allow_USB_Debugging](/images/Allow_USB_Debugging.png)

    - Check the option ‘Always allow from this computer’ and click Ok
    - Check that your device is being detected by `adb`
    - Once you accept the authorization shown above you will see the display on you screen like this.

        ![adb_Device_Detected](/images/adb_Device_Detected.png)

    - If you do not accept the message will say that the device is unauthorized

## Generate the log

- Try to reproduce the error. For e.g if Music app is crashing then run it again. The app you are   
      facing an issue with will crash.

- Now Run this command at the command prompt / console

    ```shell
    $ adb logcat -d > logcat.txt
    ```

- This will create a file called logcat.txt in the current folder.

- Check the file for any personal data. You can either scroll the log file or do a search. Remember the file can be quite large.

- Once you have the logcat file please share it with the development team as an attachment to the Gitlab error .
