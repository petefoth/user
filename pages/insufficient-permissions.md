---
layout: page
permalink: /pages/insufficient-permissions
search: exclude
title: Fix Insufficient permissions error in Linux
---

When running: <code>adb reboot bootloader</code> on your PC if you see this error

<code>Error: insufficient permissions for device</code>

**Solution**
1. Log in as root.
1. Go to folder /etc/udev/rules.d/".
1. Create file "51-android.rules".
1. Obtain vendor ID of the device 

    - Connect phone to PC via USB cable
     > Here we take the example of the Gigaset GS290 device
    - Obtain vendor ID by running lsusb in bash.

1.  This should result in a screen with a few lines of output one of which would look like this:

    <code>Bus 004 Device 004: ID 0e8d:201c MediaTek Inc</code>
    
    Here it means that the GigaSet vendor ID is 0e8d.

1. Maintain line below in file "51-android.rules"

   <code>SUBSYSTEM=="usb", ATTR{idVendor}=="0e8d", MODE="0666", GROUP="plugdev"</code> 

1. Assign read permissions on the file & reboot 

    <code>sudo chmod a+r /etc/udev/rules.d/51-android.rules</code>

Alternatively you can reload udev and the adb daemon with these commands
        <br>
        <code>
        sudo udevadm control --reload-rules
        <br>
        adb kill-server
        <br>
        adb start-server
        </code>
