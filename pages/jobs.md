---
layout: page
title: Jobs @ /e/
permalink: jobs
search: exclude
---

## Why work at /e/?

We are a **fully remote**, multicultural and international team. We enjoy working from home, from a nice _café_ or by the sea.
We have a common mission; we aim high, and we know it will take effort and time. If you are looking for a challenge… **come join us!**

## Software Engineering

The recruiting process will include at least one technical challenge and one or two interviews. 

Please write to [techjobs@e.email](mailto:techjobs@e.email)

 {:.jobs-table}

  Position  | Description
  ------------- | -------------
  Android app developer  | We are looking for experienced (5+ years) Android developers to work on various projects [Join us!](mailto:techjobs@e.email)
  Android OS developer  | Do you build and maintain a custom ROM? Can you hack into the low-level layers of AOSP? [Get in touch!](mailto:techjobs@e.email)
  QA Engineer |  We are looking for an experienced (3+ years) QA engineer for all our products (cloud & mobile). Can't sleep if a bug isn't solved? [Come squash them](mailto:techjobs@e.email)
  

## Other positions

Please write to [jobs@e.email](mailto:jobs@e.email)

  {:.jobs-table}
  
  Position   | Description
  ------------- | -------------
  Lead UI/UX designer  | we are looking for our awesome lead designer, who will help design the best mobile experience. Feeling like the new Jony Ive? [Contact us](mailto:jobs@e.email)
  Visual designer  | we are looking for experimented and talented visual designers. Like to design awesome user interfaces and icons? [Contact us](mailto:jobs@e.email)
  E-commerce sales manager |  we are looking for our eCommerce sales manager. In love with international online sales? [Contact us](mailto:jobs@e.email)
  Customer support and after sales | we are looking for our new customer support and after sales team. Love to help solve problems big or small and always with a smile? :) [Contact us](mailto:jobs@e.email)   
  Marketing team |  we are looking for marketing assistants & execs. Do you have a creative mind? [Contact us](mailto:jobs@e.email)
  Privacy specialist | we are looking for a specialist in privacy related questions, from both a technical and legal standpoint. Privacy by design your thing? [Contact us](mailto:jobs@e.email)
  Accounting assistant | we are looking for our accounting assistant who will take care of billing and payroll. Super detail-oriented and thorough? [Contact us](mailto:jobs@e.email)
  
## Important notes

- **Professional working proficiency in English** is mandatory for all positions (ILR level 3).
- All positions are **full time** if not specified otherwise.
- Please **include a CV** and ideally a cover letter in your submission.
- Due to the size of the team, **we are currently not accepting interns** or junior candidates with no previous experience.
- In 2021 **we are also looking for our CTO and CMO**. Though those positions are not yet officially opened, if you have a good track record in your field, please feel free to [contact us](mailto:jobs@e.email).
