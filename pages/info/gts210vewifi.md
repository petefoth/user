---
sidebar: home_sidebar
title: Info about Samsung Galaxy Tab S2 9.7 Wi-Fi (2016) - gts210vewifi
folder: info
layout: default
permalink: /devices/gts210vewifi
device: gts210vewifi
---
{% include templates/device_info.md %}