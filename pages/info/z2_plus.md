---
sidebar: home_sidebar
title: Info about ZUK Z2 Plus - z2_plus
folder: info
layout: default
permalink: /devices/z2_plus
device: z2_plus
---
{% include templates/device_info.md %}