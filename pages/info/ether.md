---
sidebar: home_sidebar
title: Info about Nextbit Robin - ether
folder: info
layout: default
permalink: /devices/ether
device: ether
---
{% include templates/device_info.md %}