---
sidebar: home_sidebar
title: Info about HTC One (M8) Dual SIM - m8d
folder: info
layout: default
permalink: /devices/m8d
device: m8d
---
{% include templates/device_info.md %}