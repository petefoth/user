---
sidebar: home_sidebar
title: Info about Sony  Z5 Compact - suzuran
folder: info
layout: default
permalink: /devices/suzuran
device: suzuran
---
e/OS on the Xperia Z5 Compact is based the [unofficial LineageOS 17.1 ROM](https://forum.xda-developers.com/t/rom-unofficial-10-q-lineageos-17-1-for-z5c-suzuran.4052973/) made by Bernhard Thoben ([`Professor-Berni` on github](https://github.com/Professor-Berni))

{% include templates/device_info.md %}
