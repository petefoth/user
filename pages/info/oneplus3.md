---
sidebar: home_sidebar
title: Info about OnePlus 3/3T - oneplus3
folder: info
layout: default
permalink: /devices/oneplus3
device: oneplus3
---
{% include templates/device_info.md %}
