---
sidebar: home_sidebar
title: Info about Lenovo Yoga Tab 3 Plus (YTX703L)
folder: info
layout: default
permalink: /devices/YTX703L
device: YTX703L
---
{% include templates/device_info.md %}