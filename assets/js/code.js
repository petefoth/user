
var selectedFilters = {}

function updateFilters() {
 $(".smartphone-table tr").filter(function () {
     $(this).toggle(true)
   });
 for(var column in selectedFilters){
   value = selectedFilters[column].value.toLowerCase();
   $(".smartphone-table tr ." + column).filter(function () {
     var lineValue = $(this).text().toLowerCase()
     if(selectedFilters[column].type=="truncate"){
       lineValue = Math.trunc(parseFloat(lineValue))
     }

     if((selectedFilters[column].type=="text" && lineValue.indexOf(value) < 0 
     || lineValue != value && selectedFilters[column].type=="truncate")
     && value != -1){
     //  console.log("hiding "+value)
       $(this.closest('tr')).toggle(false)
     }
   });
 }
 
}

function addFilterSelector(selectorId, colomnClass) {
 $("#" + selectorId).change(function (event) {
   var value = event.target.value;
   selectedFilters[colomnClass] = {}
   selectedFilters[colomnClass].value = value
   selectedFilters[colomnClass].type = "text"
   updateFilters()
 })
}

function addFilterTruncate(selectorId, colomnClass) {
 $("#" + selectorId).change(function (event) {
   var value = event.target.value;
   selectedFilters[colomnClass] = {}
   selectedFilters[colomnClass].value = value
   selectedFilters[colomnClass].type = "truncate"
   updateFilters()
 })
}

function addFilterRadio(radioName, colomnClass) {
 $('input[type=radio][name=' + radioName + ']').change(function (event) {
   var value = event.target.value;
   selectedFilters[colomnClass] = {}
   selectedFilters[colomnClass].type = "text"
   selectedFilters[colomnClass].value = value
   updateFilters()
 })
}
$(document).ready(function() {
 $("#myInput").on("keyup", function() {
   var value = $(this).val().toLowerCase();
   $(".smartphone-table tr").filter(function() {
     $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
   });
 });

$('.custom-select').change(function(){
 var txtSearch = "";
 txtSearch = txtSearch +  $(this).find('option:selected').text();
//              alert(txtSearch);
});




addFilterSelector("brand", "brand");
addFilterSelector("release", "release");
addFilterTruncate("size", "size");
addFilterSelector("region", "region");
addFilterSelector("channel", "channel");
addFilterSelector("maturity", "maturity");
addFilterSelector("install", "install");

addFilterRadio("battery", "battery");
addFilterRadio("sim", "sim");


$('#btnReset').on('click', function(){
  clear_form_elements("device-search");
  clear_form_elements("myCollapse");
  $("#myInput").keyup();

});

});

function clear_form_elements(class_name) {
  $("."+class_name).find(':input').each(function() {
    switch(this.type) {
        case 'text':
        case 'select-multiple':
            $(this).val('');
            break;
        case 'radio':
            this.checked = false;
            break;
    }
  });
  selectedFilters = {};
  updateFilters();
}
